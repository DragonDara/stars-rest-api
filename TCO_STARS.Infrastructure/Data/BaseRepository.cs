﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TCO_STARS.Core.Entities;
using TCO_STARS.Core.Interfaces;

namespace TCO_STARS.Infrastructure.Data
{
    public class BaseRepository : IRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IQueryable Find<T>() where T : BaseEntity
        {
            return _dbContext.Set<T>().AsNoTracking();
        }
        public IQueryable<T> FindByCondition<T>(Expression<Func<T, bool>> expression) where T : BaseEntity
        {
            return _dbContext.Set<T>().Where(expression).AsNoTracking();
        }
    }
}
