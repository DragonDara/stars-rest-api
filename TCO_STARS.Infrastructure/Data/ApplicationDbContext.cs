﻿using Microsoft.EntityFrameworkCore;
using TCO_STARS.Core.Entities;
using TCO_STARS.Core.Mappings;

namespace TCO_STARS.Infrastructure.Data
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Abreqnodoc> Abreqnodocs { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Areareport> Areareports { get; set; }
        public virtual DbSet<Areareportlink> Areareportlinks { get; set; }
        public virtual DbSet<Asbuiltlink> Asbuiltlinks { get; set; }
        public virtual DbSet<Checkfileslink> Checkfileslinks { get; set; }
        public virtual DbSet<Checklist> Checklists { get; set; }
        public virtual DbSet<Commdossier> Commdossiers { get; set; }
        public virtual DbSet<Commdossierdoc> Commdossierdocs { get; set; }
        public virtual DbSet<Commelpsp> Commelpsps { get; set; }
        public virtual DbSet<Commswdoc> Commswdocs { get; set; }
        public virtual DbSet<Completionstatus> Completionstatuses { get; set; }
        public virtual DbSet<Componentdocnolink> Componentdocnolinks { get; set; }
        public virtual DbSet<Componentno> Componentnos { get; set; }
        public virtual DbSet<Component> Components { get; set; }
        public virtual DbSet<Discipline> Disciplines { get; set; }
        public virtual DbSet<Docfilelink> Docfilelinks { get; set; }
        public virtual DbSet<Docsad> Docsads { get; set; }
        public virtual DbSet<Documentno> Documentnos { get; set; }
        public virtual DbSet<Dossierreview> Dossierreviews { get; set; }
        public virtual DbSet<Engdoc> Engdocs { get; set; }
        public virtual DbSet<Engdoctype> Engdoctypes { get; set; }
        public virtual DbSet<Fcrqvdarchive> Fcrqvdarchives { get; set; }
        public virtual DbSet<Fcrtpparchive> Fcrtpparchives { get; set; }
        public virtual DbSet<Fcrtpparchivelink> Fcrtpparchivelinks { get; set; }
        public virtual DbSet<Flsdoctype> Flsdoctypes { get; set; }
        public virtual DbSet<Form141> Form141s { get; set; }
        public virtual DbSet<IbArchive> IbArchives { get; set; }
        public virtual DbSet<IbTppArchive> IbTppArchives { get; set; }
        public virtual DbSet<Leakdoc> Leakdocs { get; set; }
        public virtual DbSet<PassportNew> PassportNews { get; set; }
        public virtual DbSet<Peer> Peers { get; set; }
        public virtual DbSet<Peerdocnolink> Peerdocnolinks { get; set; }
        public virtual DbSet<Pfdcontractor> Pfdcontractors { get; set; }
        public virtual DbSet<Phase5docnolink> Phase5docnolinks { get; set; }
        public virtual DbSet<Pmsdocdetail> Pmsdocdetails { get; set; }
        public virtual DbSet<Pmssystem> Pmssystems { get; set; }
        public virtual DbSet<Procdoc> Procdocs { get; set; }
        public virtual DbSet<Procedure> Procedures { get; set; }
        public virtual DbSet<Proctype> Proctypes { get; set; }
        public virtual DbSet<Punchlist> Punchlists { get; set; }
        public virtual DbSet<Punchlistspost> Punchlistsposts { get; set; }
        public virtual DbSet<Qvdarchive> Qvdarchives { get; set; }
        public virtual DbSet<Racert> Racerts { get; set; }
        public virtual DbSet<Racertsorg> Racertsorgs { get; set; }
        public virtual DbSet<Racertsystem> Racertsystems { get; set; }
        public virtual DbSet<Racitation> Racitations { get; set; }
        public virtual DbSet<Racitationfile> Racitationfiles { get; set; }
        public virtual DbSet<Reportdoc> Reportdocs { get; set; }
        public virtual DbSet<Scopedrwgrev> Scopedrwgrevs { get; set; }
        public virtual DbSet<Scopedrwgrevsyslink> Scopedrwgrevsyslinks { get; set; }
        public virtual DbSet<Scopedrwgsheet> Scopedrwgsheets { get; set; }
        public virtual DbSet<Shelldoc> Shelldocs { get; set; }
        public virtual DbSet<Sositem> Sositems { get; set; }
        public virtual DbSet<SosPeople> SosPeoples { get; set; }
        public virtual DbSet<SosSystem> SosSystems { get; set; }
        public virtual DbSet<Srcdoc> Srcdocs { get; set; }
        public virtual DbSet<Systema> Systems { get; set; }
        public virtual DbSet<Tpparchive> Tpparchives { get; set; }
        public virtual DbSet<Tpparchivelink> Tpparchivelinks { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Valveworkshop> Valveworkshops { get; set; }
        public virtual DbSet<Variation> Variations { get; set; }
        public virtual DbSet<Vendordoc> Vendordocs { get; set; }
        public virtual DbSet<Vendordoctype> Vendordoctypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=starsarchive-dev-cvx.database.windows.net;Initial Catalog=starsarchive-dev-cvx;User ID=dbadmin-dev;Password=mogOcrWRUP-#c;Encrypt=True;Connection Timeout=60;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("test");
            modelBuilder.ApplyConfiguration(new AbreqnodocsConfig());
            modelBuilder.ApplyConfiguration(new AreaConfig());
            modelBuilder.ApplyConfiguration(new AreareportConfig());
            modelBuilder.ApplyConfiguration(new AreareportlinkConfig());
            modelBuilder.ApplyConfiguration(new AsbuiltlinkConfig());
            modelBuilder.ApplyConfiguration(new CheckfileslinkConfig());
            modelBuilder.ApplyConfiguration(new ChecklistsConfig());
            modelBuilder.ApplyConfiguration(new CommdossierConfig());
            modelBuilder.ApplyConfiguration(new CommdossierdocConfig());
            modelBuilder.ApplyConfiguration(new CommelpspConfig());
            modelBuilder.ApplyConfiguration(new CommswdocsConfig());
            modelBuilder.ApplyConfiguration(new CompletionstatusConfig());
            modelBuilder.ApplyConfiguration(new ComponentdocnolinkConfig());
            modelBuilder.ApplyConfiguration(new ComponentnoConfig());
            modelBuilder.ApplyConfiguration(new ComponentsConfig());
            modelBuilder.ApplyConfiguration(new DisciplineConfig());
            modelBuilder.ApplyConfiguration(new DocfilelinksConfig());
            modelBuilder.ApplyConfiguration(new DocsadConfig());
            modelBuilder.ApplyConfiguration(new DocumentnoConfig());
            modelBuilder.ApplyConfiguration(new DossierreviewConfig());
            modelBuilder.ApplyConfiguration(new EngdocsConfig());
            modelBuilder.ApplyConfiguration(new EngdoctypesConfig());
            modelBuilder.ApplyConfiguration(new FcrqvdarchiveConfig());
            modelBuilder.ApplyConfiguration(new FcrtpparchiveConfig());
            modelBuilder.ApplyConfiguration(new FcrtpparchivelinkConfig());
            modelBuilder.ApplyConfiguration(new FlsdoctypeConfig());
            modelBuilder.ApplyConfiguration(new Form141Config());
            modelBuilder.ApplyConfiguration(new IbArchiveConfig());
            modelBuilder.ApplyConfiguration(new IbTppArchiveConfig());
            modelBuilder.ApplyConfiguration(new LeakdocsConfig());
            modelBuilder.ApplyConfiguration(new PassportNewConfig());
            modelBuilder.ApplyConfiguration(new PeerConfig());
            modelBuilder.ApplyConfiguration(new PeerdocnolinkConfig());
            modelBuilder.ApplyConfiguration(new PfdcontractorConfig());
            modelBuilder.ApplyConfiguration(new Phase5docnolinkConfig());
            modelBuilder.ApplyConfiguration(new PmsdocdetailConfig());
            modelBuilder.ApplyConfiguration(new PmssystemsConfig());
            modelBuilder.ApplyConfiguration(new ProcdocsConfig());
            modelBuilder.ApplyConfiguration(new ProceduresConfig());
            modelBuilder.ApplyConfiguration(new ProctypeConfig());
            modelBuilder.ApplyConfiguration(new PunchlistsConfig());
            modelBuilder.ApplyConfiguration(new PunchlistspostConfig());
            modelBuilder.ApplyConfiguration(new QvdarchiveConfig());
            modelBuilder.ApplyConfiguration(new RacertsConfig());
            modelBuilder.ApplyConfiguration(new RacertsorgsConfig());
            modelBuilder.ApplyConfiguration(new RacertsystemsConfig());
            modelBuilder.ApplyConfiguration(new RacitationConfig());
            modelBuilder.ApplyConfiguration(new RacitationfileConfig());
            modelBuilder.ApplyConfiguration(new ReportdocsConfig());
            modelBuilder.ApplyConfiguration(new ScopedrwgrevConfig());
            modelBuilder.ApplyConfiguration(new ScopedrwgrevsyslinkConfig());
            modelBuilder.ApplyConfiguration(new ScopedrwgsheetConfig());
            modelBuilder.ApplyConfiguration(new ShelldocsConfig());
            modelBuilder.ApplyConfiguration(new SositemsConfig());
            modelBuilder.ApplyConfiguration(new SosPeopleConfig());
            modelBuilder.ApplyConfiguration(new SosSystemConfig());
            modelBuilder.ApplyConfiguration(new SrcdocsConfig());
            modelBuilder.ApplyConfiguration(new SystemConfig());
            modelBuilder.ApplyConfiguration(new TpparchiveConfig());
            modelBuilder.ApplyConfiguration(new TpparchivelinkConfig());
            modelBuilder.ApplyConfiguration(new UnitConfig());
            modelBuilder.ApplyConfiguration(new UsersConfig());
            modelBuilder.ApplyConfiguration(new ValveworkshopConfig());
            modelBuilder.ApplyConfiguration(new VariationConfig());
            modelBuilder.ApplyConfiguration(new VendordocsConfig());
            modelBuilder.ApplyConfiguration(new VendordoctypesConfig());
        }
    }
}
