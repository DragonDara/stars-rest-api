using System;
using System.Linq;

namespace TCO_STARS.Infrastructure.Data
{
    public interface IEntriesProvider
    {
        IQueryable<T> Entries<T>() where T : class;
    }

    public class EntriesProvider : IEntriesProvider
    {
        private readonly ApplicationDbContext context;
        public EntriesProvider(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IQueryable<T> Entries<T>() where T : class => context.Set<T>().AsQueryable();
    }
}