using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class AreaConfig : IEntityTypeConfiguration<Area>
    {
        public void Configure(EntityTypeBuilder<Area> entity)
        {
            entity.ToTable("AREA", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Aoc1).HasColumnName("AOC1");

            entity.Property(e => e.Aoc2).HasColumnName("AOC2");

            entity.Property(e => e.Areaview)
                .HasColumnName("AREAVIEW")
                .HasMaxLength(800);

            entity.Property(e => e.Closed).HasColumnName("CLOSED");

            entity.Property(e => e.Commdate).HasColumnName("COMMDATE");

            entity.Property(e => e.Commreports)
                .HasColumnName("COMMREPORTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(500);

            entity.Property(e => e.Descrus)
                .HasColumnName("DESCRUS")
                .HasMaxLength(500);

            entity.Property(e => e.Name)
                .HasColumnName("NAME")
                .HasMaxLength(50);

            entity.Property(e => e.Namerus)
                .HasColumnName("NAMERUS")
                .HasMaxLength(50);

            entity.Property(e => e.Pcogendocs)
                .HasColumnName("PCOGENDOCS")
                .HasMaxLength(2000);

            entity.Property(e => e.Responsible)
                .HasColumnName("RESPONSIBLE")
                .HasMaxLength(200);

            entity.Property(e => e.Schdocid).HasColumnName("SCHDOCID");

            entity.Property(e => e.Schedules)
                .HasColumnName("SCHEDULES")
                .HasMaxLength(2000);

        }
    }
}
