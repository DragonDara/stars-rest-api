using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class RacitationfileConfig : IEntityTypeConfiguration<Racitationfile>
    {
        public void Configure(EntityTypeBuilder<Racitationfile> entity)
        {
            entity.ToTable("RACITATIONFILE", "test");

            entity.HasIndex(e => e.Citationid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Citationfile)
                .HasColumnName("CITATIONFILE")
                .HasMaxLength(1000);

            entity.Property(e => e.Citationid).HasColumnName("CITATIONID");

            entity.Property(e => e.Statusfile)
                .HasColumnName("STATUSFILE")
                .HasMaxLength(1000);

            entity.HasOne(d => d.Citation)
                .WithMany(p => p.Racitationfile)
                .HasForeignKey(d => d.Citationid);

        }
    }
}
