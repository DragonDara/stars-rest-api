using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ChecklistsConfig : IEntityTypeConfiguration<Checklist>
    {
        public void Configure(EntityTypeBuilder<Checklist> entity)
        {
            entity.ToTable("CHECKLISTS", "test");

            entity.HasIndex(e => e.Checksheet);

            entity.HasIndex(e => e.Itemstatus);

            entity.HasIndex(e => e.Tagno);

            entity.HasIndex(e => new { e.Systemid, e.Discipline });

            entity.HasIndex(e => new { e.Systemid, e.Tagno, e.Checksheet, e.Discipline })
                .IsUnique()
                .HasFilter("([SYSTEMID] IS NOT NULL AND [TAGNO] IS NOT NULL AND [CHECKSHEET] IS NOT NULL AND [DISCIPLINE] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Asheetcomplete)
                .HasColumnName("ASHEETCOMPLETE")
                .HasMaxLength(50);

            entity.Property(e => e.Checksheet)
                .HasColumnName("CHECKSHEET")
                .HasMaxLength(200);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(500);

            entity.Property(e => e.Createddate).HasColumnName("CREATEDDATE");

            entity.Property(e => e.Createdweek).HasColumnName("CREATEDWEEK");

            entity.Property(e => e.Datecompleted).HasColumnName("DATECOMPLETED");

            entity.Property(e => e.Discipline).HasColumnName("DISCIPLINE");

            entity.Property(e => e.Engstatus)
                .HasColumnName("ENGSTATUS")
                .HasMaxLength(50);

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1000);

            entity.Property(e => e.Itemstatus)
                .HasColumnName("ITEMSTATUS")
                .HasMaxLength(200);

            entity.Property(e => e.Nameinsp)
                .HasColumnName("NAMEINSP")
                .HasMaxLength(200);

            entity.Property(e => e.Remediation)
                .HasColumnName("REMEDIATION")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Servdesc)
                .HasColumnName("SERVDESC")
                .HasMaxLength(200);

            entity.Property(e => e.Sheetdesc)
                .HasColumnName("SHEETDESC")
                .HasMaxLength(200);

            entity.Property(e => e.Starsstatus).HasColumnName("STARSSTATUS");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(200);

            entity.Property(e => e.TppFlag).HasColumnName("TPP_FLAG");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.Checklists)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
