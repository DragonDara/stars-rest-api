using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class QvdarchiveConfig : IEntityTypeConfiguration<Qvdarchive>
    {
        public void Configure(EntityTypeBuilder<Qvdarchive> entity)
        {
            entity.ToTable("QVDARCHIVE", "test");

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Tagno);

            entity.HasIndex(e => e.Updateuserid);

            entity.HasIndex(e => new { e.Checksheet, e.Tagno });

            entity.HasIndex(e => new { e.Systemid, e.Disciplineid });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Checksheet)
                .HasColumnName("CHECKSHEET")
                .HasMaxLength(200);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(400);

            entity.Property(e => e.Completeit).HasColumnName("COMPLETEIT");

            entity.Property(e => e.Disciplineid).HasColumnName("DISCIPLINE");

            entity.Property(e => e.Docno)
                .HasColumnName("DOCNO")
                .HasMaxLength(300);

            entity.Property(e => e.Drawing)
                .HasColumnName("DRAWING")
                .HasMaxLength(200);

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1200);

            entity.Property(e => e.Notreq).HasColumnName("NOTREQ");

            entity.Property(e => e.Sheetdesc)
                .HasColumnName("SHEETDESC")
                .HasMaxLength(200);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tabno)
                .HasColumnName("TABNO")
                .HasMaxLength(100);

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(200);

            entity.Property(e => e.Tidelinkputinstarsdate).HasColumnName("TIDELINKPUTINSTARSDATE");

            entity.Property(e => e.Transferredtottidedate).HasColumnName("TRANSFERREDTOTTIDEDATE");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Verifiedby).HasColumnName("VERIFIEDBY");

            entity.Property(e => e.Verifiedstatus).HasColumnName("VERIFIEDSTATUS");

            entity.HasOne(d => d.Discipline)
                .WithMany(p => p.Qvdarchive)
                .HasForeignKey(d => d.Disciplineid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Qvdarchive)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Qvdarchive)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.VerifiedUser)
                .WithMany(p => p.QvdarchiveVerified)
                .HasForeignKey(d => d.Verifiedby)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
