using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ComponentsConfig : IEntityTypeConfiguration<Component>
    {
        public void Configure(EntityTypeBuilder<Component> entity)
        {
            entity.ToTable("COMPONENTS", "test");

            entity.HasIndex(e => e.Loopid);

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Tagid);

            entity.HasIndex(e => new { e.Systemid, e.Tagid, e.Loopid, e.Parenttagid })
                .IsUnique()
                .HasFilter("([SYSTEMID] IS NOT NULL AND [TAGID] IS NOT NULL AND [LOOPID] IS NOT NULL AND [PARENTTAGID] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Loopid).HasColumnName("LOOPID");

            entity.Property(e => e.Parenttagid).HasColumnName("PARENTTAGID");

            entity.Property(e => e.Starsstatus).HasColumnName("STARSSTATUS");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagid).HasColumnName("TAGID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Loop)
                .WithMany(p => p.LoopComponents)
                .HasForeignKey(d => d.Loopid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.ParentComponent)
                .WithMany(p => p.ChildComponents)
                .HasForeignKey(d => d.Parenttagid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Components)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Componentno)
                .WithMany(p => p.Components)
                .HasForeignKey(d => d.Tagid);


        }
    }
}
