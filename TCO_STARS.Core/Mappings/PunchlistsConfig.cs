using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PunchlistsConfig : IEntityTypeConfiguration<Punchlist>
    {
        public void Configure(EntityTypeBuilder<Punchlist> entity)
        {
            entity.ToTable("PUNCHLISTS", "test");

            entity.HasIndex(e => e.Disciplineid);

            entity.HasIndex(e => e.Plnumber)
                .IsUnique()
                .HasFilter("([PLNUMBER] IS NOT NULL)");

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Actiongroup)
                .HasColumnName("ACTIONGROUP")
                .HasMaxLength(50);

            entity.Property(e => e.Addedby)
                .HasColumnName("ADDEDBY")
                .HasMaxLength(50);

            entity.Property(e => e.Aoccomments)
                .HasColumnName("AOCCOMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Aocdate).HasColumnName("AOCDATE");

            entity.Property(e => e.Aocid).HasColumnName("AOCID");

            entity.Property(e => e.Applicabledocno)
                .HasColumnName("APPLICABLEDOCNO")
                .HasMaxLength(500);

            entity.Property(e => e.Category).HasColumnName("CATEGORY");

            entity.Property(e => e.Clearedby)
                .HasColumnName("CLEAREDBY")
                .HasMaxLength(50);

            entity.Property(e => e.Cleareddate).HasColumnName("CLEAREDDATE");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Commentseng)
                .HasColumnName("COMMENTSENG")
                .HasMaxLength(1500);

            entity.Property(e => e.Commentsrus)
                .HasColumnName("COMMENTSRUS")
                .HasMaxLength(1500);

            entity.Property(e => e.Contractor)
                .HasColumnName("CONTRACTOR")
                .HasMaxLength(200);

            entity.Property(e => e.Createddate).HasColumnName("CREATEDDATE");

            entity.Property(e => e.Disciplineid).HasColumnName("DISCIPLINEID");

            entity.Property(e => e.Eng)
                .HasColumnName("ENG")
                .HasMaxLength(50);

            entity.Property(e => e.Managercomments)
                .HasColumnName("MANAGERCOMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Managerdate).HasColumnName("MANAGERDATE");

            entity.Property(e => e.Managerid).HasColumnName("MANAGERID");

            entity.Property(e => e.Mat)
                .HasColumnName("MAT")
                .HasMaxLength(50);

            entity.Property(e => e.Pldescription)
                .HasColumnName("PLDESCRIPTION")
                .HasMaxLength(1000);

            entity.Property(e => e.Pldescrus)
                .HasColumnName("PLDESCRUS")
                .HasMaxLength(1000);

            entity.Property(e => e.Plnumber).HasColumnName("PLNUMBER");

            entity.Property(e => e.Raisedby)
                .HasColumnName("RAISEDBY")
                .HasMaxLength(200);

            entity.Property(e => e.Rejectiondate).HasColumnName("REJECTIONDATE");

            entity.Property(e => e.Rejectionreason)
                .HasColumnName("REJECTIONREASON")
                .HasMaxLength(1000);

            entity.Property(e => e.Rejectionuserid).HasColumnName("REJECTIONUSERID");

            entity.Property(e => e.Reopencomments)
                .HasColumnName("REOPENCOMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Status)
                .HasColumnName("STATUS")
                .HasMaxLength(50);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Verifiedby)
                .HasColumnName("VERIFIEDBY")
                .HasMaxLength(50);

            entity.Property(e => e.Verifieddate).HasColumnName("VERIFIEDDATE");

            entity.HasOne(d => d.Discipline)
                .WithMany(p => p.Punchlists)
                .HasForeignKey(d => d.Disciplineid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Punchlists)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
