using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ScopedrwgsheetConfig : IEntityTypeConfiguration<Scopedrwgsheet>
    {
        public void Configure(EntityTypeBuilder<Scopedrwgsheet> entity)
        {
            entity.ToTable("SCOPEDRWGSHEET", "test");

            entity.HasIndex(e => e.Sdlastrevid)
                .IsUnique()
                .HasFilter("([SDLASTREVID] IS NOT NULL)");

            entity.HasIndex(e => new { e.Docnoid, e.Sdlastrevid });

            entity.HasIndex(e => new { e.Id, e.Docnoid })
                .IsUnique();

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Sdlastrevid).HasColumnName("SDLASTREVID");

            entity.Property(e => e.Sheetno)
                .HasColumnName("SHEETNO")
                .HasMaxLength(20);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Scopedrwgsheet)
                .HasForeignKey(d => d.Docnoid);

        }
    }
}
