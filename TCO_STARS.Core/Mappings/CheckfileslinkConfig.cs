using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class CheckfileslinkConfig : IEntityTypeConfiguration<Checkfileslink>
    {
        public void Configure(EntityTypeBuilder<Checkfileslink> entity)
        {
            entity.ToTable("CHECKFILESLINK", "test");

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Checklistid).HasColumnName("CHECKLISTID");

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1000);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagid).HasColumnName("TAGID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.Checkfileslink)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
