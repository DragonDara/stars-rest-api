using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class Phase5docnolinkConfig : IEntityTypeConfiguration<Phase5docnolink>
    {
        public void Configure(EntityTypeBuilder<Phase5docnolink> entity)
        {
            entity.ToTable("PHASE5DOCNOLINK", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Phase5id).HasColumnName("PHASE5ID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Phase5docnolink)
                .HasForeignKey(d => d.Docnoid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
