using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class VendordocsConfig : IEntityTypeConfiguration<Vendordoc>
    {
        public void Configure(EntityTypeBuilder<Vendordoc> entity)
        {
            entity.ToTable("VENDORDOCS", "test");

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Tcodocnoid);

            entity.HasIndex(e => new { e.Systemid, e.Tcodocnoid })
                .IsUnique();

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tcodocnoid).HasColumnName("TCODOCNOID");

            entity.HasOne(d => d.Tcodocno)
                .WithMany(p => p.Vendordocs)
                .HasForeignKey(d => d.Tcodocnoid)
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Vendordocs)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
