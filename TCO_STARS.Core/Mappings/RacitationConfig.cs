using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class RacitationConfig : IEntityTypeConfiguration<Racitation>
    {
        public void Configure(EntityTypeBuilder<Racitation> entity)
        {
            entity.ToTable("RACITATION", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Agency)
                .HasColumnName("AGENCY")
                .HasMaxLength(50);

            entity.Property(e => e.Areaid).HasColumnName("AREAID");

            entity.Property(e => e.Areamanager)
                .HasColumnName("AREAMANAGER")
                .HasMaxLength(1000);

            entity.Property(e => e.Clearpr)
                .HasColumnName("CLEARPR")
                .HasMaxLength(200);

            entity.Property(e => e.Closed)
                .HasColumnName("CLOSED")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Coc)
                .HasColumnName("COC")
                .HasMaxLength(100);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Contact)
                .HasColumnName("CONTACT")
                .HasMaxLength(200);

            entity.Property(e => e.Department)
                .HasColumnName("DEPARTMENT")
                .HasMaxLength(200);

            entity.Property(e => e.Inspector)
                .HasColumnName("INSPECTOR")
                .HasMaxLength(200);

            entity.Property(e => e.Issuedate)
                .HasColumnName("ISSUEDATE")
                .HasMaxLength(100);

            entity.Property(e => e.Itemno)
                .HasColumnName("ITEMNO")
                .HasMaxLength(50);

            entity.Property(e => e.Letterdate)
                .HasColumnName("LETTERDATE")
                .HasMaxLength(100);

            entity.Property(e => e.Raperson)
                .HasColumnName("RAPERSON")
                .HasMaxLength(200);

            entity.Property(e => e.So)
                .HasColumnName("SO")
                .HasMaxLength(100);

            entity.Property(e => e.Status).HasColumnName("STATUS");

            entity.Property(e => e.Statuspen)
                .HasColumnName("STATUSPEN")
                .HasMaxLength(2000);

            entity.Property(e => e.Tcno)
                .HasColumnName("TCNO")
                .HasMaxLength(200);

            entity.Property(e => e.Type)
                .HasColumnName("TYPE")
                .HasMaxLength(200);

            entity.Property(e => e.Updatedate)
                .HasColumnName("UPDATEDATE")
                .HasMaxLength(100);

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(e => e.Area)
                .WithMany(a => a.Racitations)
                .HasForeignKey(e => e.Areaid);
        }
    }
}
