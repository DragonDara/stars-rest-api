using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class UsersConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.HasKey(e => e.Userid);

            entity.ToTable("USERS", "test");

            entity.HasIndex(e => e.Usercai)
                .IsUnique()
                .HasFilter("([USERCAI] IS NOT NULL)");

            entity.HasIndex(e => e.Usergroupid);

            entity.Property(e => e.Userid).HasColumnName("USERID");

            entity.Property(e => e.Badge)
                .HasColumnName("BADGE")
                .HasMaxLength(100);

            entity.Property(e => e.Canapprove)
                .HasColumnName("CANAPPROVE")
                .HasMaxLength(50);

            entity.Property(e => e.Closed).HasColumnName("CLOSED");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(100);

            entity.Property(e => e.CyrName)
                .HasColumnName("CYR_NAME")
                .HasMaxLength(200);

            entity.Property(e => e.Email)
                .HasColumnName("EMAIL")
                .HasMaxLength(100);

            entity.Property(e => e.Fullname)
                .HasColumnName("FULLNAME")
                .HasMaxLength(200);

            entity.Property(e => e.LastLogonDate).HasColumnName("LAST_LOGON_DATE");

            entity.Property(e => e.Location)
                .HasColumnName("LOCATION")
                .HasMaxLength(200);

            entity.Property(e => e.Password)
                .HasColumnName("PASSWORD")
                .HasMaxLength(50);

            entity.Property(e => e.SpeakLang)
                .HasColumnName("SPEAK_LANG")
                .HasMaxLength(50);

            entity.Property(e => e.Tcopositionid).HasColumnName("TCOPOSITIONID");

            entity.Property(e => e.Trainingstatus)
                .HasColumnName("TRAININGSTATUS")
                .HasMaxLength(100);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Usercai)
                .HasColumnName("USERCAI")
                .HasMaxLength(50);

            entity.Property(e => e.Usergroupid).HasColumnName("USERGROUPID");

            entity.Property(e => e.Usertypeid).HasColumnName("USERTYPEID");

        }
    }
}
