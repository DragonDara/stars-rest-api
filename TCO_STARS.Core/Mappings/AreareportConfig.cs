using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class AreareportConfig : IEntityTypeConfiguration<Areareport>
    {
        public void Configure(EntityTypeBuilder<Areareport> entity)
        {
            entity.ToTable("AREAREPORT", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(300);

            entity.Property(e => e.Descrus)
                .HasColumnName("DESCRUS")
                .HasMaxLength(500);

            entity.Property(e => e.Name)
                .HasColumnName("NAME")
                .HasMaxLength(100);

            entity.Property(e => e.Namerus)
                .HasColumnName("NAMERUS")
                .HasMaxLength(100);

            entity.Property(e => e.Reports)
                .HasColumnName("REPORTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Schedules)
                .HasColumnName("SCHEDULES")
                .HasMaxLength(2000);

        }
    }
}
