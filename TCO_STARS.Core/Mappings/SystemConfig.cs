using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class SystemConfig : IEntityTypeConfiguration<Systema>
    {
        public void Configure(EntityTypeBuilder<Systema> entity)
        {
            entity.ToTable("SYSTEM", "test");

            entity.HasKey(e => e.Systemid);

            entity.HasIndex(e => e.Closed);

            entity.HasIndex(e => e.Systemno)
                .IsUnique();

            entity.HasIndex(e => e.Unitid);

            entity.HasIndex(e => e.Virtual);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Allreqna).HasColumnName("ALLREQNA");

            entity.Property(e => e.Aoccomdocnoid).HasColumnName("AOCCOMDOCNOID");

            entity.Property(e => e.Aoccomments)
                .HasColumnName("AOCCOMMENTS")
                .HasMaxLength(700);

            entity.Property(e => e.Aocmccomments)
                .HasColumnName("AOCMCCOMMENTS")
                .HasMaxLength(700);

            entity.Property(e => e.Aocmcdocnoid).HasColumnName("AOCMCDOCNOID");

            entity.Property(e => e.Areano)
                .HasColumnName("AREANO")
                .HasMaxLength(50);

            entity.Property(e => e.Certnumexists)
                .HasColumnName("CERTNUMEXISTS")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Closed)
                .HasColumnName("CLOSED")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Comma).HasColumnName("COMMA");

            entity.Property(e => e.Commdosstatus).HasColumnName("COMMDOSSTATUS");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(200);

            entity.Property(e => e.Commentsforfw)
                .HasColumnName("COMMENTSFORFW")
                .HasMaxLength(700);

            entity.Property(e => e.Commf).HasColumnName("COMMF");

            entity.Property(e => e.Commfdate).HasColumnName("COMMFDATE");

            entity.Property(e => e.Commp).HasColumnName("COMMP");

            entity.Property(e => e.Commsdate).HasColumnName("COMMSDATE");

            entity.Property(e => e.Compstatus).HasColumnName("COMPSTATUS");

            entity.Property(e => e.Compvercomments)
                .HasColumnName("COMPVERCOMMENTS")
                .HasMaxLength(700);

            entity.Property(e => e.Compvercommentslink)
                .HasColumnName("COMPVERCOMMENTSLINK")
                .HasMaxLength(1000);

            entity.Property(e => e.Ct).HasColumnName("CT");

            entity.Property(e => e.Ctconfsent).HasColumnName("CTCONFSENT");

            entity.Property(e => e.Ctna).HasColumnName("CTNA");

            entity.Property(e => e.Ctnadocnoid).HasColumnName("CTNADOCNOID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(500);

            entity.Property(e => e.Descrus)
                .HasColumnName("DESCRUS")
                .HasMaxLength(500);

            entity.Property(e => e.Fifwdate).HasColumnName("FIFWDATE");

            entity.Property(e => e.Fwdate).HasColumnName("FWDATE");

            entity.Property(e => e.Fwf).HasColumnName("FWF");

            entity.Property(e => e.Fwna).HasColumnName("FWNA");

            entity.Property(e => e.Fwp).HasColumnName("FWP");

            entity.Property(e => e.Fwreq).HasColumnName("FWREQ");

            entity.Property(e => e.Fwsdate).HasColumnName("FWSDATE");

            entity.Property(e => e.Iselectrical)
                .HasColumnName("ISELECTRICAL")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Keys)
                .HasColumnName("KEYS")
                .HasMaxLength(100);

            entity.Property(e => e.Mc).HasColumnName("MC");

            entity.Property(e => e.Mccomments)
                .HasColumnName("MCCOMMENTS")
                .HasMaxLength(700);

            entity.Property(e => e.Mcconfsent).HasColumnName("MCCONFSENT");

            entity.Property(e => e.Mcna).HasColumnName("MCNA");

            entity.Property(e => e.Ninwdate).HasColumnName("NINWDATE");

            entity.Property(e => e.Notproject).HasColumnName("NOTPROJECT");

            entity.Property(e => e.Nwna).HasColumnName("NWNA");

            entity.Property(e => e.Pissuedate).HasColumnName("PISSUEDATE");

            entity.Property(e => e.Procna).HasColumnName("PROCNA");

            entity.Property(e => e.Qvdna).HasColumnName("QVDNA");

            entity.Property(e => e.Ra).HasColumnName("RA");

            entity.Property(e => e.Racertna)
                .HasColumnName("RACERTNA")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Rastatus)
                .HasColumnName("RASTATUS")
                .HasMaxLength(500);

            entity.Property(e => e.Rct).HasColumnName("RCT");

            entity.Property(e => e.Rctaccept).HasColumnName("RCTACCEPT");

            entity.Property(e => e.Rctar).HasColumnName("RCTAR");

            entity.Property(e => e.Rctf).HasColumnName("RCTF");

            entity.Property(e => e.Rctp).HasColumnName("RCTP");

            entity.Property(e => e.Rfp).HasColumnName("RFP");

            entity.Property(e => e.Rmc).HasColumnName("RMC");

            entity.Property(e => e.Rmcaccept).HasColumnName("RMCACCEPT");

            entity.Property(e => e.Rmcar).HasColumnName("RMCAR");

            entity.Property(e => e.Sdna).HasColumnName("SDNA");

            entity.Property(e => e.Secareaid).HasColumnName("SECAREAID");

            entity.Property(e => e.Sos).HasColumnName("SOS");

            entity.Property(e => e.Spares)
                .HasColumnName("SPARES")
                .HasMaxLength(100);

            entity.Property(e => e.Specialtools)
                .HasColumnName("SPECIALTOOLS")
                .HasMaxLength(100);

            entity.Property(e => e.Systemno)
                .IsRequired()
                .HasColumnName("SYSTEMNO")
                .HasMaxLength(100);

            entity.Property(e => e.Tcdate).HasColumnName("TCDATE");

            entity.Property(e => e.Tcna).HasColumnName("TCNA");

            entity.Property(e => e.Tcsdate).HasColumnName("TCSDATE");

            entity.Property(e => e.Technicalcommissionsid).HasColumnName("TECHNICALCOMMISSIONSID");

            entity.Property(e => e.Tobesigned)
                .HasColumnName("TOBESIGNED")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Todate).HasColumnName("TODATE");

            entity.Property(e => e.Transferredtotide).HasColumnName("TRANSFERREDTOTIDE");

            entity.Property(e => e.Unitdesc)
                .HasColumnName("UNITDESC")
                .HasMaxLength(1000);

            entity.Property(e => e.Unitid).HasColumnName("UNITID");

            entity.Property(e => e.Unitno)
                .HasColumnName("UNITNO")
                .HasMaxLength(50);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Veredapplicable).HasColumnName("VEREDAPPLICABLE");

            entity.Property(e => e.Virtual).HasColumnName("VIRTUAL");

            entity.Property(e => e.Vkaapplicable).HasColumnName("VKAAPPLICABLE");

            entity.Property(e => e.Ctdocnoid).HasColumnName("CTDOCNO2ID");

            entity.Property(e => e.Mcdocnoid).HasColumnName("MCDOCNO2ID");

            entity.HasOne(e => e.Aoccomdocno)
                .WithMany(e => e.AoccomSystems)
                .HasForeignKey(e => e.Aoccomdocnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(e => e.Aocmcdocno)
                .WithMany(e => e.AocmcSystems)
                .HasForeignKey(e => e.Aocmcdocnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(e => e.Ctdocno)
                .WithMany(e => e.CtSystems)
                .HasForeignKey(e => e.Ctdocnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(e => e.Mcdocno)
                .WithMany(e => e.McSystems)
                .HasForeignKey(e => e.Mcdocnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(e => e.Unit)
                .WithMany(e => e.Systems)
                .HasForeignKey(e => e.Unitid)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
