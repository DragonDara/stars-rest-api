using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class EngdoctypesConfig : IEntityTypeConfiguration<Engdoctype>
    {
        public void Configure(EntityTypeBuilder<Engdoctype> entity)
        {
            entity.ToTable("ENGDOCTYPES", "test");

            entity.HasIndex(e => e.Abreq);

            entity.HasIndex(e => new { e.Id, e.Abreq })
                .IsUnique();

            entity.HasIndex(e => new { e.Id, e.Priority });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Abreq)
                .HasColumnName("ABREQ")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Abreqdoc)
                .HasColumnName("ABREQDOC")
                .HasMaxLength(50);

            entity.Property(e => e.Cdtype)
                .HasColumnName("CDTYPE")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Compdoc)
                .HasColumnName("COMPDOC")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Doctype)
                .HasColumnName("DOCTYPE")
                .HasMaxLength(50);

            entity.Property(e => e.Priority).HasColumnName("PRIORITY");

            entity.Property(e => e.Timing)
                .HasColumnName("TIMING")
                .HasMaxLength(100);

            entity.Property(e => e.Title)
                .HasColumnName("TITLE")
                .HasMaxLength(200);

        }
    }
}
