using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class EngdocsConfig : IEntityTypeConfiguration<Engdoc>
    {
        public void Configure(EntityTypeBuilder<Engdoc> entity)
        {
            entity.ToTable("ENGDOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => new { e.Systemid, e.Docnoid })
                .IsUnique();

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Engdocs)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Engdocs)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
