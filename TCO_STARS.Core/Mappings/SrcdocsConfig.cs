using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class SrcdocsConfig : IEntityTypeConfiguration<Srcdoc>
    {
        public void Configure(EntityTypeBuilder<Srcdoc> entity)
        {
            entity.ToTable("SRCDOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Srcdocs)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Srcdocs)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
