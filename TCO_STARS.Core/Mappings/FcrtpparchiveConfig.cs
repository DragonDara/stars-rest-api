using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class FcrtpparchiveConfig : IEntityTypeConfiguration<Fcrtpparchive>
    {
        public void Configure(EntityTypeBuilder<Fcrtpparchive> entity)
        {
            entity.ToTable("FCRTPPARCHIVE", "test");

            entity.HasIndex(e => e.Tagno);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Accepteddate).HasColumnName("ACCEPTEDDATE");

            entity.Property(e => e.Addcomments)
                .HasColumnName("ADDCOMMENTS")
                .HasMaxLength(4000);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Discipline).HasColumnName("DISCIPLINE");

            entity.Property(e => e.Drawingno)
                .HasColumnName("DRAWINGNO")
                .HasMaxLength(4000);

            entity.Property(e => e.Drawingno2)
                .HasColumnName("DRAWINGNO2")
                .HasMaxLength(200);

            entity.Property(e => e.Linenumber)
                .HasColumnName("LINENUMBER")
                .HasMaxLength(4000);

            entity.Property(e => e.Receiveddate).HasColumnName("RECEIVEDDATE");

            entity.Property(e => e.Sendsigndate).HasColumnName("SENDSIGNDATE");

            entity.Property(e => e.Sendtopfd).HasColumnName("SENDTOPFD");

            entity.Property(e => e.Showdrwg)
                .HasColumnName("SHOWDRWG")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Signoffdate).HasColumnName("SIGNOFFDATE");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(200);

            entity.Property(e => e.Tcopipeowner)
                .HasColumnName("TCOPIPEOWNER")
                .HasMaxLength(200);

            entity.Property(e => e.Tidelinkputinstarsdate).HasColumnName("TIDELINKPUTINSTARSDATE");

            entity.Property(e => e.Tmpf).HasColumnName("TMPF");

            entity.Property(e => e.Transmitdate).HasColumnName("TRANSMITDATE");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(e => e.System)
                    .WithMany(s => s.Fcrtpparchives)
                    .HasForeignKey(e => e.Systemid)
                    .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
