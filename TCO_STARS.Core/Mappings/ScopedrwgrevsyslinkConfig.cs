using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ScopedrwgrevsyslinkConfig : IEntityTypeConfiguration<Scopedrwgrevsyslink>
    {
        public void Configure(EntityTypeBuilder<Scopedrwgrevsyslink> entity)
        {
            entity.HasKey(e => new { e.Systemid, e.Scopedrwgrevid });

            entity.ToTable("SCOPEDRWGREVSYSLINK", "test");

            entity.HasIndex(e => e.Scopedrwgrevid);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Scopedrwgrevid).HasColumnName("SCOPEDRWGREVID");

            entity.HasOne(d => d.Scopedrwgrev)
                .WithMany(p => p.Scopedrwgrevsyslink)
                .HasForeignKey(d => d.Scopedrwgrevid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Scopedrwgrevsyslink)
                .HasForeignKey(d => d.Systemid);
        }
    }
}
