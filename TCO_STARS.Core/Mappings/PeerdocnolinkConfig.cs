using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PeerdocnolinkConfig : IEntityTypeConfiguration<Peerdocnolink>
    {
        public void Configure(EntityTypeBuilder<Peerdocnolink> entity)
        {
            entity.ToTable("PEERDOCNOLINK", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Peerid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Peerid).HasColumnName("PEERID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Peerdocnolink)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.Peer)
                .WithMany(p => p.Peerdocnolink)
                .HasForeignKey(d => d.Peerid);

        }
    }
}
