using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ComponentdocnolinkConfig : IEntityTypeConfiguration<Componentdocnolink>
    {
        public void Configure(EntityTypeBuilder<Componentdocnolink> entity)
        {
            entity.ToTable("COMPONENTDOCNOLINK", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Tagid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Tagid).HasColumnName("TAGID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Componentdocnolink)
                .HasForeignKey(d => d.Docnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Tag)
                .WithMany(p => p.Componentdocnolink)
                .HasForeignKey(d => d.Tagid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
