using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ShelldocsConfig : IEntityTypeConfiguration<Shelldoc>
    {
        public void Configure(EntityTypeBuilder<Shelldoc> entity)
        {
            entity.ToTable("SHELLDOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Shelldocs)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Shelldocs)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
