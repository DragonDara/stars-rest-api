using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ValveworkshopConfig : IEntityTypeConfiguration<Valveworkshop>
    {
        public void Configure(EntityTypeBuilder<Valveworkshop> entity)
        {
            entity.ToTable("VALVEWORKSHOP", "test");

            entity.HasIndex(e => e.Disciplineid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Booklocation)
                .HasColumnName("BOOKLOCATION")
                .HasMaxLength(100);

            entity.Property(e => e.Certdate).HasColumnName("CERTDATE");

            entity.Property(e => e.Classification)
                .HasColumnName("CLASSIFICATION")
                .HasMaxLength(100);

            entity.Property(e => e.Disciplineid).HasColumnName("DISCIPLINEID");

            entity.Property(e => e.Docno)
                .HasColumnName("DOCNO")
                .HasMaxLength(100);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1000);

            entity.Property(e => e.Jobno)
                .HasColumnName("JOBNO")
                .HasMaxLength(50);

            entity.Property(e => e.Location)
                .HasColumnName("LOCATION")
                .HasMaxLength(50);

            entity.Property(e => e.Manufacturer)
                .HasColumnName("MANUFACTURER")
                .HasMaxLength(100);

            entity.Property(e => e.Model)
                .HasColumnName("MODEL")
                .HasMaxLength(100);

            entity.Property(e => e.Nextcertdate).HasColumnName("NEXTCERTDATE");

            entity.Property(e => e.Serialno)
                .HasColumnName("SERIALNO")
                .HasMaxLength(200);

            entity.Property(e => e.Transferredtotide).HasColumnName("TRANSFERREDTOTIDE");

            entity.Property(e => e.Valveno)
                .HasColumnName("VALVENO")
                .HasMaxLength(100);

            entity.HasOne(d => d.Discipline)
                .WithMany(p => p.Valveworkshop)
                .HasForeignKey(d => d.Disciplineid);

        }
    }
}
