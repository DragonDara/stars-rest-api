using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class IbArchiveConfig : IEntityTypeConfiguration<IbArchive>
    {
        public void Configure(EntityTypeBuilder<IbArchive> entity)
        {
            entity.ToTable("IB_ARCHIVE", "test");

            entity.HasIndex(e => e.Contractid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Booklocation)
                .HasColumnName("BOOKLOCATION")
                .HasMaxLength(100);

            entity.Property(e => e.Clientdocno)
                .HasColumnName("CLIENTDOCNO")
                .HasMaxLength(500);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Contractid)
                .HasColumnName("CONTRACTID")
                .HasMaxLength(100);

            entity.Property(e => e.Pfdno)
                .HasColumnName("PFDNO")
                .HasMaxLength(500);

            entity.Property(e => e.Status)
                .HasColumnName("STATUS")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Title)
                .HasColumnName("TITLE")
                .HasMaxLength(2000);

            entity.Property(e => e.Unitid).HasColumnName("UNITID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Contract)
                .WithMany(p => p.IbArchive)
                .HasForeignKey(d => d.Contractid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.IbArchive)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
