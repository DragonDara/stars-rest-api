using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class SositemsConfig : IEntityTypeConfiguration<Sositem>
    {
        public void Configure(EntityTypeBuilder<Sositem> entity)
        {
            entity.ToTable("SOSITEMS", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .IsRequired()
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(600);

            entity.Property(e => e.Descrus)
                .IsRequired()
                .HasColumnName("DESCRUS")
                .HasMaxLength(600);

            entity.Property(e => e.Sositemeng)
                .IsRequired()
                .HasColumnName("SOSITEM")
                .HasMaxLength(300);

            entity.Property(e => e.Sositemrus)
                .IsRequired()
                .HasColumnName("SOSITEMRUS")
                .HasMaxLength(300);

        }
    }
}
