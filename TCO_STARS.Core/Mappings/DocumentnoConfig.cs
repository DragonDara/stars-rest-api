using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class DocumentnoConfig : IEntityTypeConfiguration<Documentno>
    {
        public void Configure(EntityTypeBuilder<Documentno> entity)
        {
            entity.ToTable("DOCUMENTNO", "test");

            entity.HasIndex(e => e.Clientdocno);

            entity.HasIndex(e => e.Docno)
                .IsUnique();

            entity.HasIndex(e => e.Doctypeid);

            entity.HasIndex(e => e.Letuserid);

            entity.HasIndex(e => e.Updateuserid);

            entity.HasIndex(e => new { e.Docno, e.Clientdocno })
                .IsUnique()
                .HasFilter("([CLIENTDOCNO] IS NOT NULL)");

            entity.HasIndex(e => new { e.Id, e.Doctypeid })
                .IsUnique();

            entity.HasIndex(e => new { e.Issuestatus, e.Doctypeid });

            entity.HasIndex(e => new { e.Id, e.Doctypeid, e.Fis });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Abreq)
                .HasColumnName("ABREQ")
                .HasMaxLength(10);

            entity.Property(e => e.Actiondoc)
                .HasColumnName("ACTIONDOC")
                .HasMaxLength(10);

            entity.Property(e => e.Backdrafted).HasColumnName("BACKDRAFTED");

            entity.Property(e => e.Bkdrft)
                .HasColumnName("BKDRFT")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Clientdocno)
                .HasColumnName("CLIENTDOCNO")
                .HasMaxLength(400);

            entity.Property(e => e.Contractor)
                .HasColumnName("CONTRACTOR")
                .HasMaxLength(100);

            entity.Property(e => e.Disc)
                .HasColumnName("DISC")
                .HasMaxLength(20);

            entity.Property(e => e.Docno)
                .IsRequired()
                .HasColumnName("DOCNO")
                .HasMaxLength(200);

            entity.Property(e => e.Doctype)
                .HasColumnName("DOCTYPE")
                .HasMaxLength(50);

            entity.Property(e => e.DoctypeengVen)
                .HasColumnName("DOCTYPEENG_VEN")
                .HasMaxLength(1);

            entity.Property(e => e.Doctypeid).HasColumnName("DOCTYPEID");

            entity.Property(e => e.Fis)
                .HasColumnName("FIS")
                .HasMaxLength(50);

            entity.Property(e => e.Fischangedoc).HasColumnName("FISCHANGEDOC");

            entity.Property(e => e.Hcreceived)
                .HasColumnName("HCRECEIVED")
                .HasMaxLength(50);

            entity.Property(e => e.Hcreq)
                .HasColumnName("HCREQ")
                .HasMaxLength(50);

            entity.Property(e => e.Hostatus)
                .HasColumnName("HOSTATUS")
                .HasMaxLength(50);

            entity.Property(e => e.IbFlag).HasColumnName("IB_FLAG");

            entity.Property(e => e.Ibdoctype)
                .HasColumnName("IBDOCTYPE")
                .HasMaxLength(100);

            entity.Property(e => e.Intide)
                .HasColumnName("INTIDE")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Issuestatus)
                .HasColumnName("ISSUESTATUS")
                .HasMaxLength(50);

            entity.Property(e => e.Keydoc)
                .HasColumnName("KEYDOC")
                .HasMaxLength(10);

            entity.Property(e => e.Letissab).HasColumnName("LETISSAB");

            entity.Property(e => e.Letuserid).HasColumnName("LETUSERID");

            entity.Property(e => e.Locator)
                .HasColumnName("LOCATOR")
                .HasMaxLength(100);

            entity.Property(e => e.Mocno)
                .HasColumnName("MOCNO")
                .HasMaxLength(2000);

            entity.Property(e => e.Palastrev)
                .HasColumnName("PALASTREV")
                .HasMaxLength(50);

            entity.Property(e => e.Pkgitem)
                .HasColumnName("PKGITEM")
                .HasMaxLength(2000);

            entity.Property(e => e.Pono)
                .HasColumnName("PONO")
                .HasMaxLength(50);

            entity.Property(e => e.ReadyToBd)
                .HasColumnName("READY_TO_BD")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Sysidentna).HasColumnName("SYSIDENTNA");

            entity.Property(e => e.SysmocFlag)
                .HasColumnName("SYSMOC_FLAG")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Systemno)
                .HasColumnName("SYSTEMNO")
                .HasMaxLength(4000);

            entity.Property(e => e.TcoReissuedDocNo1)
                .HasColumnName("TCO_REISSUED_DOC_NO")
                .HasMaxLength(100);

            entity.Property(e => e.TcoReissuedDocno).HasColumnName("TCO_REISSUED_DOCNO");

            entity.Property(e => e.Title)
                .HasColumnName("TITLE")
                .HasMaxLength(800);

            entity.Property(e => e.Titlerus)
                .HasColumnName("TITLERUS")
                .HasMaxLength(800);

            entity.Property(e => e.Titletranslated)
                .HasColumnName("TITLETRANSLATED")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Toreq)
                .HasColumnName("TOREQ")
                .HasMaxLength(10);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Vendor)
                .HasColumnName("VENDOR")
                .HasMaxLength(150);

            entity.HasOne(d => d.DoctypeNavigation)
                .WithMany(p => p.Documentno)
                .HasForeignKey(d => d.Doctypeid)
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Doctype1)
                .WithMany(p => p.Documentno)
                .HasForeignKey(d => d.Doctypeid);

            entity.HasOne(d => d.Letuser)
                .WithMany(p => p.DocumentnoLetuser)
                .HasForeignKey(d => d.Letuserid);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.DocumentnoUpdateuser)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
