using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ComponentnoConfig : IEntityTypeConfiguration<Componentno>
    {
        public void Configure(EntityTypeBuilder<Componentno> entity)
        {
            entity.ToTable("COMPONENTNO", "test");

            entity.HasIndex(e => e.Disciplineid);

            entity.HasIndex(e => e.Drawingno);

            entity.HasIndex(e => e.Tagno);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(200);

            entity.Property(e => e.Disciplineid).HasColumnName("DISCIPLINEID");

            entity.Property(e => e.Drawingno)
                .HasColumnName("DRAWINGNO")
                .HasMaxLength(200);

            entity.Property(e => e.Engstatus)
                .HasColumnName("ENGSTATUS")
                .HasMaxLength(50);

            entity.Property(e => e.Pono)
                .HasColumnName("PONO")
                .HasMaxLength(200);

            entity.Property(e => e.Remark)
                .HasColumnName("REMARK")
                .HasMaxLength(500);

            entity.Property(e => e.Servdesc)
                .HasColumnName("SERVDESC")
                .HasMaxLength(500);

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(50);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Updflag).HasColumnName("UPDFLAG");

            entity.HasOne(d => d.Discipline)
                .WithMany(p => p.Componentno)
                .HasForeignKey(d => d.Disciplineid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
