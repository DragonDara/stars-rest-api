using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class AreareportlinkConfig : IEntityTypeConfiguration<Areareportlink>
    {
        public void Configure(EntityTypeBuilder<Areareportlink> entity)
        {
            entity.ToTable("AREAREPORTLINK", "test");

            entity.HasIndex(e => e.Areareportid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Areareportid).HasColumnName("AREAREPORTID");

            entity.Property(e => e.Priorityno).HasColumnName("PRIORITYNO");

            entity.Property(e => e.Subpriorityno).HasColumnName("SUBPRIORITYNO");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Areareport)
                .WithMany(p => p.Areareportlink)
                .HasForeignKey(d => d.Areareportid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Areareportlink)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
