using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class VendordoctypesConfig : IEntityTypeConfiguration<Vendordoctype>
    {
        public void Configure(EntityTypeBuilder<Vendordoctype> entity)
        {
            entity.ToTable("VENDORDOCTYPES", "test");

            entity.HasIndex(e => new { e.Id, e.Kd, e.Ad, e.Abreq })
                .IsUnique()
                .HasFilter("([KD] IS NOT NULL AND [AD] IS NOT NULL AND [ABREQ] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Abreq)
                .HasColumnName("ABREQ")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Abreqdoc)
                .IsRequired()
                .HasColumnName("ABREQDOC")
                .HasMaxLength(10);

            entity.Property(e => e.Actiondoc)
                .IsRequired()
                .HasColumnName("ACTIONDOC")
                .HasMaxLength(10);

            entity.Property(e => e.Ad)
                .HasColumnName("AD")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Doctype)
                .IsRequired()
                .HasColumnName("DOCTYPE")
                .HasMaxLength(5);

            entity.Property(e => e.Kd)
                .HasColumnName("KD")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Keydoc)
                .IsRequired()
                .HasColumnName("KEYDOC")
                .HasMaxLength(10);

            entity.Property(e => e.Priority).HasColumnName("PRIORITY");

            entity.Property(e => e.Timing)
                .HasColumnName("TIMING")
                .HasMaxLength(100);

            entity.Property(e => e.Title)
                .HasColumnName("TITLE")
                .HasMaxLength(255);

            entity.Property(e => e.Titlerus)
                .HasColumnName("TITLERUS")
                .HasMaxLength(255);

        }
    }
}
