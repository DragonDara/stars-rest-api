using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class SosPeopleConfig : IEntityTypeConfiguration<SosPeople>
    {
        public void Configure(EntityTypeBuilder<SosPeople> entity)
        {
            entity.ToTable("SOS_PEOPLE", "test");

            entity.HasIndex(e => e.Sositemid);

            entity.HasIndex(e => e.Userid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Sositemid).HasColumnName("SOSITEMID");

            entity.Property(e => e.Userid).HasColumnName("USERID");

            entity.HasOne(d => d.Sositem)
                .WithMany(p => p.SosPeople)
                .HasForeignKey(d => d.Sositemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.User)
                .WithMany(p => p.SosPeople)
                .HasForeignKey(d => d.Userid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
