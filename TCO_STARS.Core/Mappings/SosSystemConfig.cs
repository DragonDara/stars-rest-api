using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class SosSystemConfig : IEntityTypeConfiguration<SosSystem>
    {
        public void Configure(EntityTypeBuilder<SosSystem> entity)
        {
            entity.ToTable("SOS_SYSTEM", "test");

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(600);

            entity.Property(e => e.Commentsrus)
                .HasColumnName("COMMENTSRUS")
                .HasMaxLength(600);

            entity.Property(e => e.Signuserid).HasColumnName("SIGNUSERID");

            entity.Property(e => e.Sositemid).HasColumnName("SOSITEMID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.HasOne(d => d.System)
                .WithMany(p => p.SosSystem)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Signuser)
                .WithMany(p => p.SosSystemSign)
                .HasForeignKey(d => d.Signuserid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Sositem)
                .WithMany(p => p.SosSystems)
                .HasForeignKey(d => d.Sositemid)
                .OnDelete(DeleteBehavior.SetNull);


        }
    }
}
