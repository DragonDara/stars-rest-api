using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class CommswdocsConfig : IEntityTypeConfiguration<Commswdoc>
    {
        public void Configure(EntityTypeBuilder<Commswdoc> entity)
        {
            entity.ToTable("COMMSWDOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(56);

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Commswdocs)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Commswdocs)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
