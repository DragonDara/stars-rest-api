using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class DocsadConfig : IEntityTypeConfiguration<Docsad>
    {
        public void Configure(EntityTypeBuilder<Docsad> entity)
        {
            entity.ToTable("DOCSAD", "test");

            entity.HasIndex(e => e.Areaid);

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Unitid);

            entity.HasIndex(e => e.Updateuserid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Abreq)
                .HasColumnName("ABREQ")
                .HasMaxLength(10);

            entity.Property(e => e.Areaid).HasColumnName("AREAID");

            entity.Property(e => e.Direction)
                .HasColumnName("DIRECTION")
                .HasMaxLength(50);

            entity.Property(e => e.Docno)
                .HasColumnName("DOCNO")
                .HasMaxLength(200);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Remark)
                .HasColumnName("REMARK")
                .HasMaxLength(300);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Unitid).HasColumnName("UNITID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Updflag).HasColumnName("UPDFLAG");

            entity.HasOne(d => d.Area)
                .WithMany(p => p.Docsad)
                .HasForeignKey(d => d.Areaid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.DocnoNavigation)
                .WithMany(p => p.Docsad)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Docsad)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Unit)
                .WithMany(p => p.Docsad)
                .HasForeignKey(d => d.Unitid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Docsad)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
