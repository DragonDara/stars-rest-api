using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class CommdossierdocConfig : IEntityTypeConfiguration<Commdossierdoc>
    {
        public void Configure(EntityTypeBuilder<Commdossierdoc> entity)
        {
            entity.ToTable("COMMDOSSIERDOC", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(500);

            entity.Property(e => e.Commgroupid).HasColumnName("COMMGROUPID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Commdossierdoc)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Commdossierdoc)
                .HasForeignKey(d => d.Systemid);

        }
    }
}
