using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PfdcontractorConfig : IEntityTypeConfiguration<Pfdcontractor>
    {
        public void Configure(EntityTypeBuilder<Pfdcontractor> entity)
        {
            entity.HasKey(e => e.Contractno);

            entity.ToTable("PFDCONTRACTOR", "test");

            entity.Property(e => e.Contractno)
                .HasColumnName("CONTRACTNO")
                .HasMaxLength(100)
                .ValueGeneratedNever();

            entity.Property(e => e.Company)
                .HasColumnName("COMPANY")
                .HasMaxLength(100);

        }
    }
}
