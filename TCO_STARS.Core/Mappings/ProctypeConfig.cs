using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ProctypeConfig : IEntityTypeConfiguration<Proctype>
    {
        public void Configure(EntityTypeBuilder<Proctype> entity)
        {
            entity.ToTable("PROCTYPE", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(100);

            entity.Property(e => e.Proctype1)
                .IsRequired()
                .HasColumnName("PROCTYPE")
                .HasMaxLength(30);

            entity.Property(e => e.Tcoproctype)
                .HasColumnName("TCOPROCTYPE")
                .HasMaxLength(30);

        }
    }
}
