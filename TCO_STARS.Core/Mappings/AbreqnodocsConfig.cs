using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class AbreqnodocsConfig : IEntityTypeConfiguration<Abreqnodoc>
    {
        public void Configure(EntityTypeBuilder<Abreqnodoc> entity)
        {
            entity.ToTable("ABREQNODOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Doctypeid).HasColumnName("DOCTYPEID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Abreqnodocs)
                .HasForeignKey(d => d.Docnoid);

        }
    }
}
