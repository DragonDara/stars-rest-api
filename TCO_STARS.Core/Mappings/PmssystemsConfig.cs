using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PmssystemsConfig : IEntityTypeConfiguration<Pmssystem>
    {
        public void Configure(EntityTypeBuilder<Pmssystem> entity)
        {
            entity.HasKey(e => e.Systemid);

            entity.ToTable("PMSSYSTEMS", "test");

            entity.Property(e => e.Systemid)
                .HasColumnName("SYSTEMID")
                .ValueGeneratedNever();

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(500);

            entity.Property(e => e.Sortorder)
                .HasColumnName("SORTORDER")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Systemno)
                .HasColumnName("SYSTEMNO")
                .HasMaxLength(100);

            entity.Property(e => e.Unit)
                .HasColumnName("UNIT")
                .HasMaxLength(200);

        }
    }
}
