using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PmsdocdetailConfig : IEntityTypeConfiguration<Pmsdocdetail>
    {
        public void Configure(EntityTypeBuilder<Pmsdocdetail> entity)
        {
            entity.ToTable("PMSDOCDETAIL", "test");

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Checksheetno)
                .HasColumnName("CHECKSHEETNO")
                .HasMaxLength(200);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(100);

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(2000);

            entity.Property(e => e.DocNo)
                .IsRequired()
                .HasColumnName("DOC_NO")
                .HasMaxLength(100);

            entity.Property(e => e.EngName)
                .HasColumnName("ENG_NAME")
                .HasMaxLength(1000);

            entity.Property(e => e.FilePath)
                .HasColumnName("FILE_PATH")
                .HasMaxLength(2000);

            entity.Property(e => e.FilePathR)
                .HasColumnName("FILE_PATH_R")
                .HasMaxLength(2000);

            entity.Property(e => e.Flsdocdetailid).HasColumnName("FLSDOCDETAILID");

            entity.Property(e => e.Flsdoctypeid).HasColumnName("FLSDOCTYPEID");

            entity.Property(e => e.Iblocation)
                .HasColumnName("IBLOCATION")
                .HasMaxLength(100);

            entity.Property(e => e.Pmsdoctype)
                .HasColumnName("PMSDOCTYPE")
                .HasMaxLength(50);

            entity.Property(e => e.RevComNo)
                .HasColumnName("REV_COM_NO")
                .HasMaxLength(10);

            entity.Property(e => e.RusName)
                .HasColumnName("RUS_NAME")
                .HasMaxLength(1000);

            entity.Property(e => e.SortOrder).HasColumnName("SORT_ORDER");

            entity.Property(e => e.Status)
                .HasColumnName("STATUS")
                .HasMaxLength(1);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(200);

            entity.Property(e => e.Transfertotide)
                .HasColumnName("TRANSFERTOTIDE")
                .HasColumnType("numeric(1, 0)")
                .HasDefaultValueSql("((0))");

            entity.HasOne(d => d.Flsdoctype)
                .WithMany(p => p.Pmsdocdetail)
                .HasForeignKey(d => d.Flsdoctypeid)
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Pmsdocdetail)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.ClientSetNull);

        }
    }
}
