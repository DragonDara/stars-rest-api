using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ScopedrwgrevConfig : IEntityTypeConfiguration<Scopedrwgrev>
    {
        public void Configure(EntityTypeBuilder<Scopedrwgrev> entity)
        {
            entity.ToTable("SCOPEDRWGREV", "test");

            entity.HasIndex(e => e.Updateuserid);

            entity.HasIndex(e => new { e.Scopedrwgsheetid, e.Tnvrev })
                .IsUnique()
                .HasFilter("([TNVREV] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Apddate).HasColumnName("APDDATE");

            entity.Property(e => e.Apduserid).HasColumnName("APDUSERID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(1000);

            entity.Property(e => e.Drwgrev)
                .HasColumnName("DRWGREV")
                .HasMaxLength(10);

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1000);

            entity.Property(e => e.Haveorig)
                .HasColumnName("HAVEORIG")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Issuedate).HasColumnName("ISSUEDATE");

            entity.Property(e => e.Scopedrwgsheetid).HasColumnName("SCOPEDRWGSHEETID");

            entity.Property(e => e.Tnvrev)
                .HasColumnName("TNVREV")
                .HasMaxLength(10);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Scopedrwgsheet)
                .WithMany(p => p.Scopedrwgrev)
                .HasForeignKey(d => d.Scopedrwgsheetid);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Scopedrwgrev)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(d => d.Apduser)
                .WithMany(p => p.ScopedrwgrevApd)
                .HasForeignKey(d => d.Apduserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
