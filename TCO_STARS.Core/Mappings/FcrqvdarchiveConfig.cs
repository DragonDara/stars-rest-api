using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class FcrqvdarchiveConfig : IEntityTypeConfiguration<Fcrqvdarchive>
    {
        public void Configure(EntityTypeBuilder<Fcrqvdarchive> entity)
        {
            entity.ToTable("FCRQVDARCHIVE", "test");

            entity.HasIndex(e => e.Tagno);

            entity.HasIndex(e => e.Updateuserid);

            entity.HasIndex(e => new { e.Checksheet, e.Tagno });

            entity.HasIndex(e => new { e.Systemid, e.DisciplineId });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Checksheet)
                .HasColumnName("CHECKSHEET")
                .HasMaxLength(200);

            entity.Property(e => e.Completeit).HasColumnName("COMPLETEIT");

            entity.Property(e => e.DisciplineId).HasColumnName("DISCIPLINE");

            entity.Property(e => e.Docno)
                .HasColumnName("DOCNO")
                .HasMaxLength(300);

            entity.Property(e => e.Drawing)
                .HasColumnName("DRAWING")
                .HasMaxLength(200);

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1200);

            entity.Property(e => e.Notreq).HasColumnName("NOTREQ");

            entity.Property(e => e.Sheetdesc)
                .HasColumnName("SHEETDESC")
                .HasMaxLength(200);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tabno)
                .HasColumnName("TABNO")
                .HasMaxLength(100);

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(200);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.Fcrqvdarchive)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Disc)
                .WithMany(p => p.Fcrqvdarchive)
                .HasForeignKey(d => d.DisciplineId)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Fcrqvdarchive)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
