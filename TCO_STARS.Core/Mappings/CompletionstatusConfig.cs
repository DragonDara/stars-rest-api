using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class CompletionstatusConfig : IEntityTypeConfiguration<Completionstatus>
    {
        public void Configure(EntityTypeBuilder<Completionstatus> entity)
        {
            entity.ToTable("COMPLETIONSTATUS", "test");

            entity.HasIndex(e => e.Systemid)
                .IsUnique()
                .HasFilter("([SYSTEMID] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Apldone).HasColumnName("APLDONE");

            entity.Property(e => e.Apldonecm).HasColumnName("APLDONECM");

            entity.Property(e => e.Apltotal).HasColumnName("APLTOTAL");

            entity.Property(e => e.Apltotalcm).HasColumnName("APLTOTALCM");

            entity.Property(e => e.Areaid).HasColumnName("AREAID");

            entity.Property(e => e.Bpldone).HasColumnName("BPLDONE");

            entity.Property(e => e.Bpldonecm).HasColumnName("BPLDONECM");

            entity.Property(e => e.Bpltotal).HasColumnName("BPLTOTAL");

            entity.Property(e => e.Bpltotalcm).HasColumnName("BPLTOTALCM");

            entity.Property(e => e.Commchecklistsdone).HasColumnName("COMMCHECKLISTSDONE");

            entity.Property(e => e.Commcheckliststotal).HasColumnName("COMMCHECKLISTSTOTAL");

            entity.Property(e => e.Commcheckloaded).HasColumnName("COMMCHECKLOADED");

            entity.Property(e => e.Commereddone).HasColumnName("COMMEREDDONE");

            entity.Property(e => e.Commvreddone).HasColumnName("COMMVREDDONE");

            entity.Property(e => e.Compnumbers).HasColumnName("COMPNUMBERS");

            entity.Property(e => e.Ereddone).HasColumnName("EREDDONE");

            entity.Property(e => e.EreddoneFpr).HasColumnName("EREDDONE_FPR");

            entity.Property(e => e.Eredtotal).HasColumnName("EREDTOTAL");

            entity.Property(e => e.EredtotalFpr).HasColumnName("EREDTOTAL_FPR");

            entity.Property(e => e.Loopsdone).HasColumnName("LOOPSDONE");

            entity.Property(e => e.Loopstotal).HasColumnName("LOOPSTOTAL");

            entity.Property(e => e.Ppdone).HasColumnName("PPDONE");

            entity.Property(e => e.Pptotal).HasColumnName("PPTOTAL");

            entity.Property(e => e.Proccount).HasColumnName("PROCCOUNT");

            entity.Property(e => e.Proccountdone).HasColumnName("PROCCOUNTDONE");

            entity.Property(e => e.Qvddone).HasColumnName("QVDDONE");

            entity.Property(e => e.Qvdtotal).HasColumnName("QVDTOTAL");

            entity.Property(e => e.Radone).HasColumnName("RADONE");

            entity.Property(e => e.Ratotal).HasColumnName("RATOTAL");

            entity.Property(e => e.Sd).HasColumnName("SD");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tppdone).HasColumnName("TPPDONE");

            entity.Property(e => e.Tpptotal).HasColumnName("TPPTOTAL");

            entity.Property(e => e.Vareaid).HasColumnName("VAREAID");

            entity.Property(e => e.Vendorkadone).HasColumnName("VENDORKADONE");

            entity.Property(e => e.Vendorkatotal).HasColumnName("VENDORKATOTAL");

            entity.Property(e => e.Vreddone).HasColumnName("VREDDONE");

            entity.Property(e => e.VreddoneFpr).HasColumnName("VREDDONE_FPR");

            entity.Property(e => e.Vredtotal).HasColumnName("VREDTOTAL");

            entity.Property(e => e.VredtotalFpr).HasColumnName("VREDTOTAL_FPR");

            entity.HasOne(d => d.System)
                .WithOne(p => p.Completionstatus)
                .HasForeignKey<Completionstatus>(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
