using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class RacertsystemsConfig : IEntityTypeConfiguration<Racertsystem>
    {
        public void Configure(EntityTypeBuilder<Racertsystem> entity)
        {
            entity.ToTable("RACERTSYSTEMS", "test");

            entity.HasIndex(e => e.Certid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Certid).HasColumnName("CERTID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Cert)
                .WithMany(p => p.Racertsystems)
                .HasForeignKey(d => d.Certid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Racertsystems)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);


        }
    }
}
