using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class FcrtpparchivelinkConfig : IEntityTypeConfiguration<Fcrtpparchivelink>
    {
        public void Configure(EntityTypeBuilder<Fcrtpparchivelink> entity)
        {
            entity.ToTable("FCRTPPARCHIVELINK", "test");

            entity.HasIndex(e => e.Tppid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Filetype).HasColumnName("FILETYPE");

            entity.Property(e => e.Tppid).HasColumnName("TPPID");

            entity.Property(e => e.Tpplink)
                .HasColumnName("TPPLINK")
                .HasMaxLength(1000);

            entity.HasOne(d => d.Tpp)
                .WithMany(p => p.Fcrtpparchivelink)
                .HasForeignKey(d => d.Tppid);

        }
    }
}
