using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class CommdossierConfig : IEntityTypeConfiguration<Commdossier>
    {
        public void Configure(EntityTypeBuilder<Commdossier> entity)
        {
            entity.ToTable("COMMDOSSIER", "test");

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Updateuserid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Elec)
                .HasColumnName("ELEC")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Mech)
                .HasColumnName("MECH")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Ops)
                .HasColumnName("OPS")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Pco)
                .HasColumnName("PCO")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.Commdossier)
                .HasForeignKey(d => d.Systemid);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Commdossier)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
