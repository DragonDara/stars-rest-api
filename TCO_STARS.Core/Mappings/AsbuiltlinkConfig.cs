using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class AsbuiltlinkConfig : IEntityTypeConfiguration<Asbuiltlink>
    {
        public void Configure(EntityTypeBuilder<Asbuiltlink> entity)
        {
            entity.ToTable("ASBUILTLINK", "test");

            entity.HasIndex(e => e.Drawingid);

            entity.HasIndex(e => e.Updateuserid);

            entity.HasIndex(e => new { e.Systemid, e.Drawingid })
                .IsUnique()
                .HasFilter("([SYSTEMID] IS NOT NULL AND [DRAWINGID] IS NOT NULL)");

            entity.HasIndex(e => new { e.Systemid, e.Drawingid, e.Redlconst, e.Redlcomm })
                .IsUnique()
                .HasFilter("([SYSTEMID] IS NOT NULL AND [DRAWINGID] IS NOT NULL AND [REDLCONST] IS NOT NULL AND [REDLCOMM] IS NOT NULL)");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Bdrftcommstatus)
                .HasColumnName("BDRFTCOMMSTATUS")
                .HasColumnType("numeric(1, 0)")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Bdrftconststatus)
                .HasColumnName("BDRFTCONSTSTATUS")
                .HasColumnType("numeric(1, 0)")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Bduserid)
                .HasColumnName("BDUSERID")
                .HasColumnType("numeric(5, 0)");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Drawingid).HasColumnName("DRAWINGID");

            entity.Property(e => e.Finalbdraftstatus)
                .HasColumnName("FINALBDRAFTSTATUS")
                .HasColumnType("numeric(1, 0)")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Flag).HasColumnName("FLAG");

            entity.Property(e => e.Globalid)
                .HasColumnName("GLOBALID")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Recievedcomm)
                .HasColumnName("RECIEVEDCOMM")
                .HasMaxLength(50);

            entity.Property(e => e.Recievedconst)
                .HasColumnName("RECIEVEDCONST")
                .HasMaxLength(50);

            entity.Property(e => e.Redlcomm).HasColumnName("REDLCOMM");

            entity.Property(e => e.Redlcommflsize)
                .HasColumnName("REDLCOMMFLSIZE")
                .HasMaxLength(20);

            entity.Property(e => e.Redlcommfn)
                .HasColumnName("REDLCOMMFN")
                .HasMaxLength(1000);

            entity.Property(e => e.Redlconst).HasColumnName("REDLCONST");

            entity.Property(e => e.Redlconstflsize)
                .HasColumnName("REDLCONSTFLSIZE")
                .HasMaxLength(20);

            entity.Property(e => e.Redlconstfn)
                .HasColumnName("REDLCONSTFN")
                .HasMaxLength(1000);

            entity.Property(e => e.Sheetno)
                .HasColumnName("SHEETNO")
                .HasMaxLength(1000);

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tidestatus).HasColumnName("TIDESTATUS");

            entity.Property(e => e.Transferredtottidedate).HasColumnName("TRANSFERREDTOTTIDEDATE");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updatedatecomm).HasColumnName("UPDATEDATECOMM");

            entity.Property(e => e.Updatedateconst).HasColumnName("UPDATEDATECONST");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.Property(e => e.Updateuseridcomm).HasColumnName("UPDATEUSERIDCOMM");

            entity.Property(e => e.Updateuseridconst).HasColumnName("UPDATEUSERIDCONST");

            entity.HasOne(d => d.Drawing)
                .WithMany(p => p.Asbuiltlink)
                .HasForeignKey(d => d.Drawingid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Asbuiltlink)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Asbuiltlink)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
