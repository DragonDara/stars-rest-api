using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class VariationConfig : IEntityTypeConfiguration<Variation>
    {
        public void Configure(EntityTypeBuilder<Variation> entity)
        {
            entity.ToTable("VARIATION", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Areaid)
                .HasColumnName("AREAID")
                .HasMaxLength(100);

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(200);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Filena)
                .HasColumnName("FILENA")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.No).HasColumnName("NO");

            entity.Property(e => e.Status)
                .HasColumnName("STATUS")
                .HasColumnType("numeric(38, 0)");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Variation)
                .HasForeignKey(d => d.Docnoid);

        }
    }
}
