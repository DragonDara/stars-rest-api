using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class UnitConfig : IEntityTypeConfiguration<Unit>
    {
        public void Configure(EntityTypeBuilder<Unit> entity)
        {
            entity.ToTable("UNIT", "test");

            entity.Property(e => e.Unitid).HasColumnName("UNITID");

            entity.Property(e => e.Areaid).HasColumnName("AREAID");

            entity.Property(e => e.Closed)
                .HasColumnName("CLOSED")
                .HasColumnType("numeric(38, 0)");

            entity.Property(e => e.Commdate).HasColumnName("COMMDATE");

            entity.Property(e => e.Unitname)
                .HasColumnName("UNITNAME")
                .HasMaxLength(255);

            entity.Property(e => e.Unitnamerus)
                .HasColumnName("UNITNAMERUS")
                .HasMaxLength(255);

            entity.Property(e => e.Unitno)
                .HasColumnName("UNITNO")
                .HasMaxLength(255);

            entity.Property(e => e.Unitnorus)
                .HasColumnName("UNITNORUS")
                .HasMaxLength(255);

            entity.Property(e => e.Virtual)
                .HasColumnName("VIRTUAL")
                .HasColumnType("numeric(38, 0)");

            entity.HasOne(e => e.Area)
                .WithMany(a => a.Units)
                .HasForeignKey(e => e.Areaid);

        }
    }
}
