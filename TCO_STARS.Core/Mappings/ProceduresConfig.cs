using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class ProceduresConfig : IEntityTypeConfiguration<Procedure>
    {
        public void Configure(EntityTypeBuilder<Procedure> entity)
        {
            entity.ToTable("PROCEDURES2", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Proctypeid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Commgroupid).HasColumnName("COMMGROUPID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Execstatusid).HasColumnName("EXECSTATUSID");

            entity.Property(e => e.Proctypeid).HasColumnName("PROCTYPEID");

            entity.Property(e => e.Statusid).HasColumnName("STATUSID");

            entity.Property(e => e.Tagid).HasColumnName("TAGID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Procedures2)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.Proctype)
                .WithMany(p => p.Procedures2)
                .HasForeignKey(d => d.Proctypeid);

        }
    }
}
