using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class DocfilelinksConfig : IEntityTypeConfiguration<Docfilelink>
    {
        public void Configure(EntityTypeBuilder<Docfilelink> entity)
        {
            entity.ToTable("DOCFILELINKS", "test");

            entity.HasIndex(e => new { e.Docnoid, e.Filetype });

            entity.HasIndex(e => new { e.Docnoid, e.Filename, e.Description });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.BackdraftStatus).HasColumnName("BACKDRAFT_STATUS");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(400);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Filename)
                .HasColumnName("FILENAME")
                .HasMaxLength(1000);

            entity.Property(e => e.Filetype).HasColumnName("FILETYPE");

            entity.Property(e => e.Rev)
                .HasColumnName("REV")
                .HasMaxLength(50);

            entity.Property(e => e.Transferredtotidedate).HasColumnName("TRANSFERREDTOTIDEDATE");

            entity.Property(e => e.Transferredtotideuserid).HasColumnName("TRANSFERREDTOTIDEUSER");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Docfilelinks)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.Transferredtotideuser)
                .WithMany(p => p.Docfilelinks)
                .HasForeignKey(d => d.Transferredtotideuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
