using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class DisciplineConfig : IEntityTypeConfiguration<Discipline>
    {
        public void Configure(EntityTypeBuilder<Discipline> entity)
        {
            entity.ToTable("DISCIPLINE", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(200);

            entity.Property(e => e.Descriptionrus)
                .HasColumnName("DESCRIPTIONRUS")
                .HasMaxLength(200);

            entity.Property(e => e.DiscGrpCode)
                .HasColumnName("DISC_GRP_CODE")
                .HasMaxLength(50);

            entity.Property(e => e.Discipline1)
                .HasColumnName("DISCIPLINE")
                .HasMaxLength(50);

        }
    }
}
