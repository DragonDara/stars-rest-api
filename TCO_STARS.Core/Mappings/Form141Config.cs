using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class Form141Config : IEntityTypeConfiguration<Form141>
    {
        public void Configure(EntityTypeBuilder<Form141> entity)
        {
            entity.ToTable("FORM141", "test");

            entity.HasIndex(e => e.Signuserid);

            entity.HasIndex(e => e.Systemid);

            entity.HasIndex(e => e.Updateuserid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Doauserid).HasColumnName("DOAUSERID");

            entity.Property(e => e.Itemid).HasColumnName("ITEMID");

            entity.Property(e => e.Newitemname)
                .HasColumnName("NEWITEMNAME")
                .HasMaxLength(200);

            entity.Property(e => e.Newitemnamerus)
                .HasColumnName("NEWITEMNAMERUS")
                .HasMaxLength(200);

            entity.Property(e => e.Personname)
                .HasColumnName("PERSONNAME")
                .HasMaxLength(200);

            entity.Property(e => e.Reqastartup).HasColumnName("REQASTARTUP");

            entity.Property(e => e.Reqbconst).HasColumnName("REQBCONST");

            entity.Property(e => e.Reqbstartup).HasColumnName("REQBSTARTUP");

            entity.Property(e => e.Signdate).HasColumnName("SIGNDATE");

            entity.Property(e => e.Signuserid).HasColumnName("SIGNUSERID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Tcodepperson)
                .HasColumnName("TCODEPPERSON")
                .HasMaxLength(200);

            entity.Property(e => e.Tcodeppersonrus)
                .HasColumnName("TCODEPPERSONRUS")
                .HasMaxLength(200);

            entity.Property(e => e.Tcopositionid).HasColumnName("TCOPOSITIONID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Signuser)
                .WithMany(p => p.Form141Signuser)
                .HasForeignKey(d => d.Signuserid);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Form141)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Updateuser)
                .WithMany(p => p.Form141Updateuser)
                .HasForeignKey(d => d.Updateuserid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
