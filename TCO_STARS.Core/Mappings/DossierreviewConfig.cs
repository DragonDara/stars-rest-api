using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class DossierreviewConfig : IEntityTypeConfiguration<Dossierreview>
    {
        public void Configure(EntityTypeBuilder<Dossierreview> entity)
        {
            entity.ToTable("DOSSIERREVIEW", "test");

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Accepteddate).HasColumnName("ACCEPTEDDATE");

            entity.Property(e => e.Closed).HasColumnName("CLOSED");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(2000);

            entity.Property(e => e.Contractorid).HasColumnName("CONTRACTORID");

            entity.Property(e => e.Location)
                .HasColumnName("LOCATION")
                .HasMaxLength(200);

            entity.Property(e => e.Pfduserid).HasColumnName("PFDUSERID");

            entity.Property(e => e.Receiveddate).HasColumnName("RECEIVEDDATE");

            entity.Property(e => e.Reviewdate).HasColumnName("REVIEWDATE");

            entity.Property(e => e.Reviewuserid).HasColumnName("REVIEWUSERID");

            entity.Property(e => e.Status).HasColumnName("STATUS");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Transmittaldate).HasColumnName("TRANSMITTALDATE");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.Dossierreview)
                .HasForeignKey(d => d.Systemid);

            entity.HasOne(d => d.Contractoruser)
                .WithMany(p => p.Dossierreviewcontractors)
                .HasForeignKey(d => d.Contractorid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Pfduser)
                .WithMany(p => p.Pfds)
                .HasForeignKey(d => d.Pfduserid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Reviewuser)
                .WithMany(p => p.Dossierreviews)
                .HasForeignKey(d => d.Reviewuserid)
                .OnDelete(DeleteBehavior.SetNull);


        }
    }
}
