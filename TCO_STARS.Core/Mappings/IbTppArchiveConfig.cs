using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class IbTppArchiveConfig : IEntityTypeConfiguration<IbTppArchive>
    {
        public void Configure(EntityTypeBuilder<IbTppArchive> entity)
        {
            entity.ToTable("IB_TPP_ARCHIVE", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(300);

            entity.Property(e => e.Contractno).HasColumnName("CONTRACTNO");

            entity.Property(e => e.Htno)
                .HasColumnName("HTNO")
                .HasMaxLength(100);

            entity.Property(e => e.Location)
                .HasColumnName("LOCATION")
                .HasMaxLength(100);

            entity.Property(e => e.Status).HasColumnName("STATUS");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.System)
                .WithMany(p => p.IbTppArchive)
                .HasForeignKey(d => d.Systemid)
                .HasConstraintName("FK_IBTPPARCHIVE_SYSTEM_SYSTEMID");

        }
    }
}
