using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PassportNewConfig : IEntityTypeConfiguration<PassportNew>
    {
        public void Configure(EntityTypeBuilder<PassportNew> entity)
        {
            entity.ToTable("PASSPORT_NEW", "test");

            entity.HasIndex(e => e.Cpdocnoid);

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Tagno);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Comments)
                .HasColumnName("COMMENTS")
                .HasMaxLength(1300);

            entity.Property(e => e.Cpdocnoid).HasColumnName("CPDOCNOID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Equipment)
                .HasColumnName("EQUIPMENT")
                .HasMaxLength(300);

            entity.Property(e => e.Permittooperate)
                .HasColumnName("PERMITTOOPERATE")
                .HasMaxLength(300);

            entity.Property(e => e.Registrdate).HasColumnName("REGISTRDATE");

            entity.Property(e => e.Tagno)
                .HasColumnName("TAGNO")
                .HasMaxLength(300);

            entity.Property(e => e.Tagnoid).HasColumnName("TAGONOID");


            entity.HasOne(d => d.Cpdocno)
                .WithMany(p => p.CpPassportNew)
                .HasForeignKey(d => d.Cpdocnoid);

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.PassportNew)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.Componentno)
                .WithMany(p => p.PassportNew)
                .HasForeignKey(d => d.Tagnoid);
        }
    }
}
