using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PunchlistspostConfig : IEntityTypeConfiguration<Punchlistspost>
    {
        public void Configure(EntityTypeBuilder<Punchlistspost> entity)
        {
            entity.HasKey(e => e.Plnumber);

            entity.ToTable("PUNCHLISTSPOST", "test");

            entity.HasIndex(e => e.Unitid);

            entity.Property(e => e.Plnumber)
                .HasColumnName("PLNUMBER")
                .HasMaxLength(50)
                .ValueGeneratedNever();

            entity.Property(e => e.Actiongroup)
                .HasColumnName("ACTIONGROUP")
                .HasMaxLength(100);

            entity.Property(e => e.Category).HasColumnName("CATEGORY");

            entity.Property(e => e.Clearedby).HasColumnName("CLEAREDBY");

            entity.Property(e => e.Cleareddate).HasColumnName("CLEAREDDATE");

            entity.Property(e => e.Contractor)
                .HasColumnName("CONTRACTOR")
                .HasMaxLength(200);

            entity.Property(e => e.Createddate).HasColumnName("CREATEDDATE");

            entity.Property(e => e.Disciplineid).HasColumnName("DISCIPLINEID");

            entity.Property(e => e.Eng)
                .HasColumnName("ENG")
                .HasMaxLength(50);

            entity.Property(e => e.Mat)
                .HasColumnName("MAT")
                .HasMaxLength(50);

            entity.Property(e => e.Pldescription)
                .HasColumnName("PLDESCRIPTION")
                .HasMaxLength(1000);

            entity.Property(e => e.Pldescrus)
                .HasColumnName("PLDESCRUS")
                .HasMaxLength(1000);

            entity.Property(e => e.Raisedby).HasColumnName("RAISEDBY");

            entity.Property(e => e.Status)
                .HasColumnName("STATUS")
                .HasMaxLength(50);

            entity.Property(e => e.Unitid).HasColumnName("UNITID");

            entity.Property(e => e.Verifiedby).HasColumnName("VERIFIEDBY");

            entity.Property(e => e.Verifieddate).HasColumnName("VERIFIEDDATE");

            entity.HasOne(d => d.Verifiedbyuser)
                .WithMany(p => p.Punchlistspostverified)
                .HasForeignKey(d => d.Verifiedby)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Unit)
                .WithMany(p => p.Punchlistspost)
                .HasForeignKey(d => d.Unitid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.Raisedbyuser)
                .WithMany(p => p.Punchlistspostraised)
                .HasForeignKey(d => d.Raisedby)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
