using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class RacertsConfig : IEntityTypeConfiguration<Racert>
    {
        public void Configure(EntityTypeBuilder<Racert> entity)
        {
            entity.ToTable("RACERTS", "test");

            entity.HasIndex(e => e.Orgid);

            entity.HasIndex(e => new { e.Docnoid, e.Certnum });

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Certnum)
                .HasColumnName("CERTNUM")
                .HasMaxLength(300);

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(2000);

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Elfilestatus).HasColumnName("ELFILESTATUS");

            entity.Property(e => e.Expiry).HasColumnName("EXPIRY");

            entity.Property(e => e.Obtained).HasColumnName("OBTAINED");

            entity.Property(e => e.Oldorgid).HasColumnName("OLDORGID");

            entity.Property(e => e.Orgid).HasColumnName("ORGID");

            entity.Property(e => e.Pono)
                .HasColumnName("PONO")
                .HasMaxLength(2000);

            entity.Property(e => e.Supplier)
                .HasColumnName("SUPPLIER")
                .HasMaxLength(1000);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Racerts)
                .HasForeignKey(d => d.Docnoid);

            entity.HasOne(d => d.Org)
                .WithMany(p => p.Racerts)
                .HasForeignKey(d => d.Orgid);

        }
    }
}
