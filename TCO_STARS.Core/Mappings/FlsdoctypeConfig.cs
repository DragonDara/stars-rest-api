using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class FlsdoctypeConfig : IEntityTypeConfiguration<Flsdoctype>
    {
        public void Configure(EntityTypeBuilder<Flsdoctype> entity)
        {
            entity.ToTable("FLSDOCTYPE", "test");

            entity.Property(e => e.Id)
                .HasColumnName("ID")
                .ValueGeneratedNever();

            entity.Property(e => e.Code)
                .HasColumnName("CODE")
                .HasMaxLength(4);

            entity.Property(e => e.Doctype)
                .HasColumnName("DOCTYPE")
                .HasMaxLength(100);

            entity.Property(e => e.Name)
                .HasColumnName("NAME")
                .HasMaxLength(500);

            entity.Property(e => e.Overall)
                .HasColumnName("OVERALL")
                .HasColumnType("numeric(38, 0)")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Sortorder)
                .HasColumnName("SORTORDER")
                .HasDefaultValueSql("((0))");

            entity.Property(e => e.Unit)
                .HasColumnName("UNIT")
                .HasMaxLength(50);

        }
    }
}
