using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class PeerConfig : IEntityTypeConfiguration<Peer>
    {
        public void Configure(EntityTypeBuilder<Peer> entity)
        {
            entity.ToTable("PEER", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Actiontoconsider)
                .HasColumnName("ACTIONTOCONSIDER")
                .HasMaxLength(2000);

            entity.Property(e => e.Actiontoconsiderrus)
                .HasColumnName("ACTIONTOCONSIDERRUS")
                .HasMaxLength(2000);

            entity.Property(e => e.Activity)
                .IsRequired()
                .HasColumnName("ACTIVITY")
                .HasMaxLength(2000);

            entity.Property(e => e.Activityrus)
                .HasColumnName("ACTIVITYRUS")
                .HasMaxLength(2000);

            entity.Property(e => e.Areaid).HasColumnName("AREAID");

            entity.Property(e => e.Areareportid).HasColumnName("AREAREPORTID");

            entity.Property(e => e.Category)
                .HasColumnName("CATEGORY")
                .HasMaxLength(10);

            entity.Property(e => e.Docforreview)
                .HasColumnName("DOCFORREVIEW")
                .HasMaxLength(2000);

            entity.Property(e => e.Docforreviewrus)
                .HasColumnName("DOCFORREVIEWRUS")
                .HasMaxLength(2000);

            entity.Property(e => e.Interviewees)
                .HasColumnName("INTERVIEWEES")
                .HasMaxLength(600);

            entity.Property(e => e.Itemno).HasColumnName("ITEMNO");

            entity.Property(e => e.Observation)
                .HasColumnName("OBSERVATION")
                .HasMaxLength(2000);

            entity.Property(e => e.Observationrus)
                .HasColumnName("OBSERVATIONRUS")
                .HasMaxLength(2000);

            entity.Property(e => e.Respnote)
                .HasColumnName("RESPNOTE")
                .HasMaxLength(2000);

            entity.Property(e => e.Respnoterus)
                .HasColumnName("RESPNOTERUS")
                .HasMaxLength(2000);

            entity.Property(e => e.Status).HasColumnName("STATUS");

            entity.Property(e => e.Subitemno).HasColumnName("SUBITEMNO");

            entity.Property(e => e.Subteam)
                .HasColumnName("SUBTEAM")
                .HasMaxLength(10);

            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

            entity.Property(e => e.Updateuserid).HasColumnName("UPDATEUSERID");

            entity.HasOne(e => e.Area)
                .WithMany(a => a.Peers)
                .HasForeignKey(e => e.Areaid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
