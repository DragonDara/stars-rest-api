using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class LeakdocsConfig : IEntityTypeConfiguration<Leakdoc>
    {
        public void Configure(EntityTypeBuilder<Leakdoc> entity)
        {
            entity.ToTable("LEAKDOCS", "test");

            entity.HasIndex(e => e.Docnoid);

            entity.HasIndex(e => e.Systemid);

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Docnoid).HasColumnName("DOCNOID");

            entity.Property(e => e.Systemid).HasColumnName("SYSTEMID");

            entity.HasOne(d => d.Docno)
                .WithMany(p => p.Leakdocs)
                .HasForeignKey(d => d.Docnoid)
                .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(d => d.System)
                .WithMany(p => p.Leakdocs)
                .HasForeignKey(d => d.Systemid)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
