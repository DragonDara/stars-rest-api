using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Mappings
{
    public class RacertsorgsConfig : IEntityTypeConfiguration<Racertsorg>
    {
        public void Configure(EntityTypeBuilder<Racertsorg> entity)
        {
            entity.ToTable("RACERTSORGS", "test");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(100);

            entity.Property(e => e.Organization)
                .HasColumnName("ORGANIZATION")
                .HasMaxLength(300);

        }
    }
}
