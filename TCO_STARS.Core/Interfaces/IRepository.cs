﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TCO_STARS.Core.Entities;

namespace TCO_STARS.Core.Interfaces
{
    public interface IRepository
    {
        IQueryable Find<T>() where T : BaseEntity;
        IQueryable<T> FindByCondition<T>(Expression<Func<T, bool>> expression) where T : BaseEntity;
    }
}
