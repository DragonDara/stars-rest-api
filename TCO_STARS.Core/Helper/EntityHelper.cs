﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TCO_STARS.Core.Helper
{
    public class EntityHelper
    {
        public static Type[] GetTypesInNamespace(string nameSpace)
        {
            return Assembly.GetExecutingAssembly().GetTypes()
                      .Where(t => t.Namespace == nameSpace)
                      .ToArray();
        }
    }
}
