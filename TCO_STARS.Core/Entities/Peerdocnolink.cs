﻿namespace TCO_STARS.Core.Entities
{
    public partial class Peerdocnolink
    {
        public int Peerid { get; set; }
        public int Docnoid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Peer Peer { get; set; }
    }
}
