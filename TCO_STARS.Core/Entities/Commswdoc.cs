﻿namespace TCO_STARS.Core.Entities
{
    public partial class Commswdoc
    {
        public int Systemid { get; set; }
        public int Docnoid { get; set; }
        public string Tagno { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Systema System { get; set; }
    }
}
