﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Unit
    {
        public Unit()
        {
            Docsad = new HashSet<Docsad>();
            Punchlistspost = new HashSet<Punchlistspost>();
            Systems = new HashSet<Systema>();
        }

        public int Unitid { get; set; }
        public string Unitno { get; set; }
        public string Unitname { get; set; }
        public int? Areaid { get; set; }
        public decimal? Closed { get; set; }
        public decimal? Virtual { get; set; }
        public string Unitnorus { get; set; }
        public string Unitnamerus { get; set; }
        public DateTime? Commdate { get; set; }

        public Area Area { get; set; }
        public ICollection<Docsad> Docsad { get; set; }
        public ICollection<Punchlistspost> Punchlistspost { get; set; }
        public ICollection<Systema> Systems { get; set; }
    }
}
