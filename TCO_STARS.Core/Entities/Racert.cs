﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Racert
    {
        public Racert()
        {
            Racertsystems = new HashSet<Racertsystem>();
        }

        public int? Orgid { get; set; }
        public string Certnum { get; set; }
        public int? Docnoid { get; set; }
        public string Description { get; set; }
        public string Pono { get; set; }
        public string Supplier { get; set; }
        public int? Oldorgid { get; set; }
        public DateTime? Obtained { get; set; }
        public DateTime? Expiry { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int? Elfilestatus { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Racertsorg Org { get; set; }
        public ICollection<Racertsystem> Racertsystems { get; set; }
    }
}
