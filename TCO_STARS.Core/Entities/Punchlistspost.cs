﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Punchlistspost
    {
        public int? Unitid { get; set; }
        public string Plnumber { get; set; }
        public int? Raisedby { get; set; }
        public int? Category { get; set; }
        public string Pldescription { get; set; }
        public int? Disciplineid { get; set; }
        public string Contractor { get; set; }
        public string Mat { get; set; }
        public string Eng { get; set; }
        public string Status { get; set; }
        public DateTime? Cleareddate { get; set; }
        public int? Clearedby { get; set; }
        public DateTime? Verifieddate { get; set; }
        public int? Verifiedby { get; set; }
        public DateTime? Createddate { get; set; }
        public string Actiongroup { get; set; }
        public string Pldescrus { get; set; }

        public Unit Unit { get; set; }
        public User Raisedbyuser { get; set; }
        public User Verifiedbyuser { get; set; }

    }
}
