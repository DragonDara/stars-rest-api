﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Punchlist
    {
        public int? Systemid { get; set; }
        public int? Plnumber { get; set; }
        public string Raisedby { get; set; }
        public int? Category { get; set; }
        public string Pldescription { get; set; }
        public int? Disciplineid { get; set; }
        public string Contractor { get; set; }
        public string Mat { get; set; }
        public string Eng { get; set; }
        public string Status { get; set; }
        public DateTime? Cleareddate { get; set; }
        public string Clearedby { get; set; }
        public DateTime? Verifieddate { get; set; }
        public string Verifiedby { get; set; }
        public DateTime? Createddate { get; set; }
        public string Actiongroup { get; set; }
        public string Pldescrus { get; set; }
        public string Addedby { get; set; }
        public string Comments { get; set; }
        public int Id { get; set; }
        public DateTime? Managerdate { get; set; }
        public string Managercomments { get; set; }
        public int? Managerid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Reopencomments { get; set; }
        public int? Aocid { get; set; }
        public string Aoccomments { get; set; }
        public DateTime? Aocdate { get; set; }
        public string Rejectionreason { get; set; }
        public DateTime? Rejectiondate { get; set; }
        public int? Rejectionuserid { get; set; }
        public string Applicabledocno { get; set; }
        public string Commentseng { get; set; }
        public string Commentsrus { get; set; }

        public Discipline Discipline { get; set; }
        public Systema System { get; set; }
    }
}
