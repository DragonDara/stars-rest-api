﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Discipline
    {
        public Discipline()
        {
            Componentno = new HashSet<Componentno>();
            Punchlists = new HashSet<Punchlist>();
            Valveworkshop = new HashSet<Valveworkshop>();
        }

        public int Id { get; set; }
        public string Discipline1 { get; set; }
        public string Description { get; set; }
        public string Descriptionrus { get; set; }
        public string DiscGrpCode { get; set; }

        public ICollection<Componentno> Componentno { get; set; }
        public ICollection<Punchlist> Punchlists { get; set; }
        public ICollection<Valveworkshop> Valveworkshop { get; set; }
        public ICollection<Fcrqvdarchive> Fcrqvdarchive { get; set; }
        public ICollection<Qvdarchive> Qvdarchive { get; set; }

    }
}
