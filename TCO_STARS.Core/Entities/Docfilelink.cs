﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Docfilelink
    {
        public int? Docnoid { get; set; }
        public string Filename { get; set; }
        public string Rev { get; set; }
        public string Description { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int? BackdraftStatus { get; set; }
        public int? Filetype { get; set; }
        public DateTime? Transferredtotidedate { get; set; }
        public int? Transferredtotideuserid { get; set; }
        public int Id { get; set; }


        public Documentno Docno { get; set; }
        public User Transferredtotideuser { get; set; }
    }
}
