﻿namespace TCO_STARS.Core.Entities
{
    public partial class Areareportlink
    {
        public int Areareportid { get; set; }
        public int Systemid { get; set; }
        public int? Priorityno { get; set; }
        public int? Subpriorityno { get; set; }
        public int Id { get; set; }

        public Areareport Areareport { get; set; }
        public Systema System { get; set; }
    }
}
