﻿namespace TCO_STARS.Core.Entities
{
    public partial class Commdossierdoc
    {
        public int? Docnoid { get; set; }
        public int? Systemid { get; set; }
        public int? Commgroupid { get; set; }
        public string Comments { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Systema System { get; set; }
    }
}
