﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Racertsorg
    {
        public Racertsorg()
        {
            Racerts = new HashSet<Racert>();
        }

        public string Organization { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }

        public ICollection<Racert> Racerts { get; set; }
    }
}
