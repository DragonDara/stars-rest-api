﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class PassportNew
    {
        public int? Docnoid { get; set; }
        public string Equipment { get; set; }
        public string Tagno { get; set; }
        public int? Tagnoid { get; set; }
        public DateTime? Registrdate { get; set; }
        public string Permittooperate { get; set; }
        public string Comments { get; set; }
        public int? Cpdocnoid { get; set; }
        public int Id { get; set; }

        public Documentno Cpdocno { get; set; }
        public Documentno Docno { get; set; }
        public Componentno Componentno { get; set; }
    }
}
