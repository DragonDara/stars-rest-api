﻿namespace TCO_STARS.Core.Entities
{
    public partial class Phase5docnolink
    {
        public int? Phase5id { get; set; }
        public int? Docnoid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
    }
}
