﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Areareport
    {
        public Areareport()
        {
            Areareportlink = new HashSet<Areareportlink>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Namerus { get; set; }
        public string Schedules { get; set; }
        public string Reports { get; set; }
        public string Descrus { get; set; }

        public ICollection<Areareportlink> Areareportlink { get; set; }
    }
}
