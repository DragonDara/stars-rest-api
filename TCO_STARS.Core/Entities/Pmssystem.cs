﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Pmssystem
    {
        public Pmssystem()
        {
            Pmsdocdetail = new HashSet<Pmsdocdetail>();
        }

        public int Systemid { get; set; }
        public string Systemno { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public int? Sortorder { get; set; }

        public ICollection<Pmsdocdetail> Pmsdocdetail { get; set; }
    }
}
