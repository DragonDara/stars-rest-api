﻿namespace TCO_STARS.Core.Entities
{
    public partial class Checkfileslink
    {
        public int Id { get; set; }
        public int? Checklistid { get; set; }
        public int? Tagid { get; set; }
        public int? Systemid { get; set; }
        public string Filename { get; set; }

        public Systema System { get; set; }
    }
}
