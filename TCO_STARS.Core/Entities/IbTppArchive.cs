﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class IbTppArchive
    {
        public string Htno { get; set; }
        public int? Systemid { get; set; }
        public string Location { get; set; }
        public int? Contractno { get; set; }
        public int? Status { get; set; }
        public string Comments { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
    }
}
