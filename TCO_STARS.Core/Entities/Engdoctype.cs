﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Engdoctype
    {
        public Engdoctype()
        {
            Documentno = new HashSet<Documentno>();
        }

        public int Id { get; set; }
        public string Doctype { get; set; }
        public string Title { get; set; }
        public string Abreqdoc { get; set; }
        public string Timing { get; set; }
        public decimal Abreq { get; set; }
        public decimal? Compdoc { get; set; }
        public int? Priority { get; set; }
        public int? Cdtype { get; set; }

        public ICollection<Documentno> Documentno { get; set; }
    }
}
