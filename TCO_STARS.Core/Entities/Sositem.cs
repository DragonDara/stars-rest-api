﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Sositem
    {
        public Sositem()
        {
            SosPeople = new HashSet<SosPeople>();
            SosSystems = new HashSet<SosSystem>();
        }

        public int Id { get; set; }
        public string Sositemeng { get; set; }
        public string Sositemrus { get; set; }
        public string Description { get; set; }
        public string Descrus { get; set; }

        public ICollection<SosPeople> SosPeople { get; set; }
        public ICollection<SosSystem> SosSystems { get; set; }
    }
}
