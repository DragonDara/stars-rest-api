﻿namespace TCO_STARS.Core.Entities
{
    public partial class Procedure
    {
        public int Docnoid { get; set; }
        public int? Tagid { get; set; }
        public int Statusid { get; set; }
        public int Proctypeid { get; set; }
        public int? Commgroupid { get; set; }
        public int Execstatusid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Proctype Proctype { get; set; }
    }
}
