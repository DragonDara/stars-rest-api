﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCO_STARS.Core.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
