﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Vendordoctype
    {
        public Vendordoctype()
        {
            Documentno = new HashSet<Documentno>();
        }

        public int Id { get; set; }
        public string Doctype { get; set; }
        public string Title { get; set; }
        public string Keydoc { get; set; }
        public string Actiondoc { get; set; }
        public string Abreqdoc { get; set; }
        public string Timing { get; set; }
        public decimal? Kd { get; set; }
        public decimal? Ad { get; set; }
        public decimal? Abreq { get; set; }
        public string Titlerus { get; set; }
        public int? Priority { get; set; }

        public ICollection<Documentno> Documentno { get; set; }
    }
}
