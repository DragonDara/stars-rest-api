﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Qvdarchive
    {
        public int? Disciplineid { get; set; }
        public string Checksheet { get; set; }
        public string Tagno { get; set; }
        public string Drawing { get; set; }
        public string Filename { get; set; }
        public int? Completeit { get; set; }
        public int? Systemid { get; set; }
        public string Sheetdesc { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Docno { get; set; }
        public string Tabno { get; set; }
        public int? Notreq { get; set; }
        public string Comments { get; set; }
        public DateTime? Transferredtottidedate { get; set; }
        public DateTime? Tidelinkputinstarsdate { get; set; }
        public int? Verifiedstatus { get; set; }
        public int? Verifiedby { get; set; }
        public int Id { get; set; }

        public Discipline Discipline { get; set; }
        public Systema System { get; set; }
        public User Updateuser { get; set; }
        public User VerifiedUser { get; set; }
    }
}
