﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Dossierreview
    {
        public int Systemid { get; set; }
        public DateTime? Reviewdate { get; set; }
        public int? Contractorid { get; set; }
        public int? Pfduserid { get; set; }
        public int? Status { get; set; }
        public DateTime? Accepteddate { get; set; }
        public DateTime? Transmittaldate { get; set; }
        public string Comments { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Receiveddate { get; set; }
        public int? Closed { get; set; }
        public int? Reviewuserid { get; set; }
        public string Location { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
        public User Contractoruser { get; set; }
        public User Reviewuser { get; set; }
        public User Pfduser { get; set; }

    }
}
