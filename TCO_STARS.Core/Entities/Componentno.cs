﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Componentno
    {
        public Componentno()
        {
            Componentdocnolink = new HashSet<Componentdocnolink>();
            Components = new HashSet<Component>();
            ChildComponents = new HashSet<Component>();
            Components = new HashSet<Component>();
            PassportNew = new HashSet<PassportNew>();
        }

        public int Id { get; set; }
        public string Tagno { get; set; }
        public string Description { get; set; }
        public string Servdesc { get; set; }
        public string Drawingno { get; set; }
        public string Pono { get; set; }
        public string Engstatus { get; set; }
        public int? Disciplineid { get; set; }
        public int? Updflag { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Remark { get; set; }

        public Discipline Discipline { get; set; }
        public ICollection<Componentdocnolink> Componentdocnolink { get; set; }
        public ICollection<Component> LoopComponents { get; set; }
        public ICollection<Component> ChildComponents { get; set; }
        public ICollection<Component> Components { get; set; }
        public ICollection<PassportNew> PassportNew { get; set; }
    }
}
