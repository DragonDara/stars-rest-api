﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Proctype
    {
        public Proctype()
        {
            Procedures2 = new HashSet<Procedure>();
        }

        public string Proctype1 { get; set; }
        public string Description { get; set; }
        public string Tcoproctype { get; set; }
        public int Id { get; set; }

        public ICollection<Procedure> Procedures2 { get; set; }
    }
}
