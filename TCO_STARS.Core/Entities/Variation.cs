﻿namespace TCO_STARS.Core.Entities
{
    public partial class Variation
    {
        public int? Docnoid { get; set; }
        public string Comments { get; set; }
        public decimal? Status { get; set; }
        public int? No { get; set; }
        public string Areaid { get; set; }
        public int? Filena { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
    }
}
