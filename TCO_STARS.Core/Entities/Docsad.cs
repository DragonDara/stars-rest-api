﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Docsad
    {
        public string Docno { get; set; }
        public int? Systemid { get; set; }
        public string Direction { get; set; }
        public int Id { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Remark { get; set; }
        public string Abreq { get; set; }
        public int Docnoid { get; set; }
        public int? Unitid { get; set; }
        public int? Areaid { get; set; }
        public int? Updflag { get; set; }

        public Area Area { get; set; }
        public Documentno DocnoNavigation { get; set; }
        public Systema System { get; set; }
        public Unit Unit { get; set; }
        public User Updateuser { get; set; }
    }
}
