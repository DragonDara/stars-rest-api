﻿namespace TCO_STARS.Core.Entities
{
    public partial class Tpparchivelink
    {
        public int? Tppid { get; set; }
        public string Tpplink { get; set; }
        public int? Filetype { get; set; }
        public int Id { get; set; }

        public Tpparchive Tpp { get; set; }
    }
}
