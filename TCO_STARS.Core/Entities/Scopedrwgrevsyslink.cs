﻿namespace TCO_STARS.Core.Entities
{
    public partial class Scopedrwgrevsyslink
    {
        public int Systemid { get; set; }
        public int Scopedrwgrevid { get; set; }

        public Scopedrwgrev Scopedrwgrev { get; set; }
        public Systema System { get; set; }
    }
}
