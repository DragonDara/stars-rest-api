﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Fcrqvdarchive
    {
        public int Id { get; set; }
        public int? DisciplineId { get; set; }
        public string Checksheet { get; set; }
        public string Tagno { get; set; }
        public string Drawing { get; set; }
        public string Filename { get; set; }
        public int? Completeit { get; set; }
        public int? Systemid { get; set; }
        public string Sheetdesc { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Docno { get; set; }
        public string Tabno { get; set; }
        public int? Notreq { get; set; }

        public Discipline Disc { get; set; }
        public Systema System { get; set; }
        public User Updateuser { get; set; }
    }
}
