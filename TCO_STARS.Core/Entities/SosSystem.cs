﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class SosSystem
    {
        public int? Sositemid { get; set; }
        public int? Systemid { get; set; }
        public int? Signuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Comments { get; set; }
        public string Commentsrus { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
        public User Signuser { get; set; }
        public Sositem Sositem { get; set; }

    }
}
