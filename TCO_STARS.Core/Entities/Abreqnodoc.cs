﻿namespace TCO_STARS.Core.Entities
{
    public partial class Abreqnodoc
    {
        public int Docnoid { get; set; }
        public string Comments { get; set; }
        public int? Doctypeid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
    }
}
