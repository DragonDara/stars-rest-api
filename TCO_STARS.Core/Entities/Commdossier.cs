﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Commdossier
    {
        public int Systemid { get; set; }
        public decimal? Pco { get; set; }
        public decimal? Elec { get; set; }
        public decimal? Mech { get; set; }
        public decimal? Ops { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
        public User Updateuser { get; set; }
    }
}
