﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Scopedrwgsheet
    {
        public Scopedrwgsheet()
        {
            Scopedrwgrev = new HashSet<Scopedrwgrev>();
        }

        public int Id { get; set; }
        public int Docnoid { get; set; }
        public string Sheetno { get; set; }
        public int? Sdlastrevid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }

        public Documentno Docno { get; set; }
        public ICollection<Scopedrwgrev> Scopedrwgrev { get; set; }
    }
}
