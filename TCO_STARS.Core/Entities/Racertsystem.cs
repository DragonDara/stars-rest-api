﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Racertsystem
    {
        public int Certid { get; set; }
        public int Systemid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int Id { get; set; }

        public Racert Cert { get; set; }
        public Systema System { get; set; }
    }
}
