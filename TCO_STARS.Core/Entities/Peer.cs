﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Peer
    {
        public Peer()
        {
            Peerdocnolink = new HashSet<Peerdocnolink>();
        }

        public int Id { get; set; }
        public int Itemno { get; set; }
        public int Subitemno { get; set; }
        public string Subteam { get; set; }
        public string Activity { get; set; }
        public string Docforreview { get; set; }
        public string Interviewees { get; set; }
        public string Observation { get; set; }
        public string Actiontoconsider { get; set; }
        public int? Status { get; set; }
        public string Activityrus { get; set; }
        public string Docforreviewrus { get; set; }
        public string Observationrus { get; set; }
        public string Actiontoconsiderrus { get; set; }
        public int Areaid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Respnote { get; set; }
        public string Respnoterus { get; set; }
        public int? Areareportid { get; set; }
        public string Category { get; set; }

        public Area Area { get; set; }

        public ICollection<Peerdocnolink> Peerdocnolink { get; set; }
    }
}
