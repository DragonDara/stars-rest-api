﻿namespace TCO_STARS.Core.Entities
{
    public partial class Racitationfile
    {
        public int? Citationid { get; set; }
        public string Citationfile { get; set; }
        public string Statusfile { get; set; }
        public int Id { get; set; }

        public Racitation Citation { get; set; }
    }
}
