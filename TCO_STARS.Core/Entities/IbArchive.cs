﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class IbArchive
    {
        public string Pfdno { get; set; }
        public string Clientdocno { get; set; }
        public string Title { get; set; }
        public string Booklocation { get; set; }
        public string Contractid { get; set; }
        public int? Status { get; set; }
        public string Comments { get; set; }
        public int? Systemid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int? Unitid { get; set; }
        public int Id { get; set; }

        public Pfdcontractor Contract { get; set; }
        public Systema System { get; set; }
    }
}
