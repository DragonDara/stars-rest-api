﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Documentno
    {
        public Documentno()
        {
            Abreqnodocs = new HashSet<Abreqnodoc>();
            Asbuiltlink = new HashSet<Asbuiltlink>();
            Commdossierdoc = new HashSet<Commdossierdoc>();
            Commelpsp = new HashSet<Commelpsp>();
            Commswdocs = new HashSet<Commswdoc>();
            Componentdocnolink = new HashSet<Componentdocnolink>();
            Docfilelinks = new HashSet<Docfilelink>();
            Docsad = new HashSet<Docsad>();
            Engdocs = new HashSet<Engdoc>();
            Leakdocs = new HashSet<Leakdoc>();
            PassportNew = new HashSet<PassportNew>();
            Peerdocnolink = new HashSet<Peerdocnolink>();
            Phase5docnolink = new HashSet<Phase5docnolink>();
            Procdocs = new HashSet<Procdoc>();
            Procedures2 = new HashSet<Procedure>();
            Racerts = new HashSet<Racert>();
            Reportdocs = new HashSet<Reportdoc>();
            Scopedrwgsheet = new HashSet<Scopedrwgsheet>();
            Shelldocs = new HashSet<Shelldoc>();
            Srcdocs = new HashSet<Srcdoc>();
            Variation = new HashSet<Variation>();
            Vendordocs = new HashSet<Vendordoc>();
            AoccomSystems = new HashSet<Systema>();
            AocmcSystems = new HashSet<Systema>();
            CtSystems = new HashSet<Systema>();
            McSystems = new HashSet<Systema>();
            CpPassportNew = new HashSet<PassportNew>();
        }

        public int Id { get; set; }
        public string Docno { get; set; }
        public string Title { get; set; }
        public string Titlerus { get; set; }
        public string Palastrev { get; set; }
        public string Fis { get; set; }
        public string Doctype { get; set; }
        public string Hostatus { get; set; }
        public string Issuestatus { get; set; }
        public string Abreq { get; set; }
        public string Keydoc { get; set; }
        public string Actiondoc { get; set; }
        public string Hcreq { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Clientdocno { get; set; }
        public string Hcreceived { get; set; }
        public string Disc { get; set; }
        public string Vendor { get; set; }
        public string Pono { get; set; }
        public string Pkgitem { get; set; }
        public string Systemno { get; set; }
        public string Mocno { get; set; }
        public int? Updateuserid { get; set; }
        public int Doctypeid { get; set; }
        public decimal? SysmocFlag { get; set; }
        public decimal? Titletranslated { get; set; }
        public string Toreq { get; set; }
        public int? Letissab { get; set; }
        public int? Letuserid { get; set; }
        public decimal? ReadyToBd { get; set; }
        public int? Fischangedoc { get; set; }
        public int? Sysidentna { get; set; }
        public string Locator { get; set; }
        public int? Backdrafted { get; set; }
        public int? IbFlag { get; set; }
        public string Ibdoctype { get; set; }
        public string Contractor { get; set; }
        public int? Bkdrft { get; set; }
        public int? Intide { get; set; }
        public int? TcoReissuedDocno { get; set; }
        public string TcoReissuedDocNo1 { get; set; }
        public string DoctypeengVen { get; set; }

        public Vendordoctype Doctype1 { get; set; }
        public Engdoctype DoctypeNavigation { get; set; }
        public User Letuser { get; set; }
        public User Updateuser { get; set; }
        public ICollection<Abreqnodoc> Abreqnodocs { get; set; }
        public ICollection<Asbuiltlink> Asbuiltlink { get; set; }
        public ICollection<Commdossierdoc> Commdossierdoc { get; set; }
        public ICollection<Commelpsp> Commelpsp { get; set; }
        public ICollection<Commswdoc> Commswdocs { get; set; }
        public ICollection<Componentdocnolink> Componentdocnolink { get; set; }
        public ICollection<Docfilelink> Docfilelinks { get; set; }
        public ICollection<Docsad> Docsad { get; set; }
        public ICollection<Engdoc> Engdocs { get; set; }
        public ICollection<Leakdoc> Leakdocs { get; set; }
        public ICollection<PassportNew> CpPassportNew { get; set; }
        public ICollection<PassportNew> PassportNew { get; set; }
        public ICollection<Peerdocnolink> Peerdocnolink { get; set; }
        public ICollection<Phase5docnolink> Phase5docnolink { get; set; }
        public ICollection<Procdoc> Procdocs { get; set; }
        public ICollection<Procedure> Procedures2 { get; set; }
        public ICollection<Racert> Racerts { get; set; }
        public ICollection<Reportdoc> Reportdocs { get; set; }
        public ICollection<Scopedrwgsheet> Scopedrwgsheet { get; set; }
        public ICollection<Shelldoc> Shelldocs { get; set; }
        public ICollection<Srcdoc> Srcdocs { get; set; }
        public ICollection<Variation> Variation { get; set; }
        public ICollection<Vendordoc> Vendordocs { get; set; }
        public ICollection<Systema> AoccomSystems { get; set; }
        public ICollection<Systema> AocmcSystems { get; set; }
        public ICollection<Systema> CtSystems { get; set; }
        public ICollection<Systema> McSystems { get; set; }

    }
}
