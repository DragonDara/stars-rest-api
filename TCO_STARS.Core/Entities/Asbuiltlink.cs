﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Asbuiltlink
    {
        public int? Systemid { get; set; }
        public int? Drawingid { get; set; }
        public int? Redlconst { get; set; }
        public int? Redlcomm { get; set; }
        public string Redlconstfn { get; set; }
        public string Redlcommfn { get; set; }
        public string Recievedconst { get; set; }
        public string Recievedcomm { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public decimal? Globalid { get; set; }
        public string Comments { get; set; }
        public string Sheetno { get; set; }
        public int? Flag { get; set; }
        public DateTime? Updatedatecomm { get; set; }
        public DateTime? Updatedateconst { get; set; }
        public int? Updateuseridconst { get; set; }
        public int? Updateuseridcomm { get; set; }
        public int? Tidestatus { get; set; }
        public string Redlconstflsize { get; set; }
        public string Redlcommflsize { get; set; }
        public DateTime? Transferredtottidedate { get; set; }
        public decimal? Bdrftcommstatus { get; set; }
        public decimal? Bdrftconststatus { get; set; }
        public decimal? Bduserid { get; set; }
        public decimal? Finalbdraftstatus { get; set; }
        public int Id { get; set; }

        public Documentno Drawing { get; set; }
        public Systema System { get; set; }
        public User Updateuser { get; set; }
    }
}
