﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TCO_STARS.Core.Entities
{
    public partial class Systema
    {
        public Systema()
        {
            Areareportlink = new HashSet<Areareportlink>();
            Asbuiltlink = new HashSet<Asbuiltlink>();
            Checkfileslink = new HashSet<Checkfileslink>();
            Checklists = new HashSet<Checklist>();
            Commdossier = new HashSet<Commdossier>();
            Commdossierdoc = new HashSet<Commdossierdoc>();
            Commelpsp = new HashSet<Commelpsp>();
            Commswdocs = new HashSet<Commswdoc>();
            Components = new HashSet<Component>();
            Docsad = new HashSet<Docsad>();
            Dossierreview = new HashSet<Dossierreview>();
            Engdocs = new HashSet<Engdoc>();
            Fcrqvdarchive = new HashSet<Fcrqvdarchive>();
            Form141 = new HashSet<Form141>();
            IbArchive = new HashSet<IbArchive>();
            IbTppArchive = new HashSet<IbTppArchive>();
            Leakdocs = new HashSet<Leakdoc>();
            Procdocs = new HashSet<Procdoc>();
            Punchlists = new HashSet<Punchlist>();
            Qvdarchive = new HashSet<Qvdarchive>();
            Reportdocs = new HashSet<Reportdoc>();
            Scopedrwgrevsyslink = new HashSet<Scopedrwgrevsyslink>();
            Shelldocs = new HashSet<Shelldoc>();
            SosSystem = new HashSet<SosSystem>();
            Srcdocs = new HashSet<Srcdoc>();
            Fcrtpparchives = new HashSet<Fcrtpparchive>();
            Racertsystems = new HashSet<Racertsystem>();
            Tpparchives = new HashSet<Tpparchive>(); 
        }

        [Key]
        public int Systemid { get; set; }
        public string Systemno { get; set; }
        public string Description { get; set; }
        public decimal? Closed { get; set; }
        public DateTime? Fifwdate { get; set; }
        public DateTime? Ninwdate { get; set; }
        public DateTime? Fwdate { get; set; }
        public DateTime? Todate { get; set; }
        public DateTime? Commsdate { get; set; }
        public DateTime? Commfdate { get; set; }
        public int? Unitid { get; set; }
        public int? Virtual { get; set; }
        public DateTime? Pissuedate { get; set; }
        public DateTime? Rfp { get; set; }
        public DateTime? Ct { get; set; }
        public DateTime? Sos { get; set; }
        public DateTime? Mc { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Specialtools { get; set; }
        public string Spares { get; set; }
        public string Keys { get; set; }
        public string Rastatus { get; set; }
        public string Descrus { get; set; }
        public int? Ra { get; set; }
        public DateTime? Tcdate { get; set; }
        public decimal? Iselectrical { get; set; }
        public DateTime? Tcsdate { get; set; }
        public int? Technicalcommissionsid { get; set; }
        public DateTime? Rct { get; set; }
        public DateTime? Rmc { get; set; }
        public DateTime? Fwf { get; set; }
        public DateTime? Fwp { get; set; }
        public DateTime? Rctf { get; set; }
        public DateTime? Rctp { get; set; }
        public DateTime? Commf { get; set; }
        public DateTime? Commp { get; set; }
        public DateTime? Comma { get; set; }
        public string Comments { get; set; }
        public DateTime? Fwsdate { get; set; }
        public decimal? Tobesigned { get; set; }
        public int? Ctdocnoid { get; set; }
        public int? Mcdocnoid { get; set; }
        public string Mccomments { get; set; }
        public int? Compstatus { get; set; }
        public string Compvercomments { get; set; }
        public int? Vkaapplicable { get; set; }
        public int? Veredapplicable { get; set; }
        public int? Commdosstatus { get; set; }
        public DateTime? Rctar { get; set; }
        public DateTime? Ctconfsent { get; set; }
        public int? Aoccomdocnoid { get; set; }
        public string Aoccomments { get; set; }
        public int? Rctaccept { get; set; }
        public DateTime? Rmcar { get; set; }
        public DateTime? Mcconfsent { get; set; }
        public int? Aocmcdocnoid { get; set; }
        public string Aocmccomments { get; set; }
        public int? Rmcaccept { get; set; }
        public string Unitno { get; set; }
        public string Areano { get; set; }
        public int? Fwreq { get; set; }
        public int? Allreqna { get; set; }
        public int? Ctna { get; set; }
        public int? Fwna { get; set; }
        public int? Tcna { get; set; }
        public int? Mcna { get; set; }
        public string Commentsforfw { get; set; }
        public int? Notproject { get; set; }
        public string Compvercommentslink { get; set; }
        public int? Qvdna { get; set; }
        public string Unitdesc { get; set; }
        public int? Procna { get; set; }
        public int? Certnumexists { get; set; }
        public int? Sdna { get; set; }
        public int? Secareaid { get; set; }
        public int? Nwna { get; set; }
        public int? Ctnadocnoid { get; set; }
        public int? Racertna { get; set; }
        public int? Transferredtotide { get; set; }

        public Documentno Ctdocno { get; set; }
        public Documentno Mcdocno { get; set; }
        public Completionstatus Completionstatus { get; set; }
        public Documentno Aoccomdocno { get; set; }
        public Documentno Aocmcdocno { get; set; }
        public Unit Unit { get; set; }

        public ICollection<Areareportlink> Areareportlink { get; set; }
        public ICollection<Asbuiltlink> Asbuiltlink { get; set; }
        public ICollection<Checkfileslink> Checkfileslink { get; set; }
        public ICollection<Checklist> Checklists { get; set; }
        public ICollection<Commdossier> Commdossier { get; set; }
        public ICollection<Commdossierdoc> Commdossierdoc { get; set; }
        public ICollection<Commelpsp> Commelpsp { get; set; }
        public ICollection<Commswdoc> Commswdocs { get; set; }
        public ICollection<Component> Components { get; set; }
        public ICollection<Docsad> Docsad { get; set; }
        public ICollection<Dossierreview> Dossierreview { get; set; }
        public ICollection<Engdoc> Engdocs { get; set; }
        public ICollection<Fcrqvdarchive> Fcrqvdarchive { get; set; }
        public ICollection<Form141> Form141 { get; set; }
        public ICollection<IbArchive> IbArchive { get; set; }
        public ICollection<IbTppArchive> IbTppArchive { get; set; }
        public ICollection<Leakdoc> Leakdocs { get; set; }
        public ICollection<Procdoc> Procdocs { get; set; }
        public ICollection<Punchlist> Punchlists { get; set; }
        public ICollection<Qvdarchive> Qvdarchive { get; set; }
        public ICollection<Reportdoc> Reportdocs { get; set; }
        public ICollection<Scopedrwgrevsyslink> Scopedrwgrevsyslink { get; set; }
        public ICollection<Shelldoc> Shelldocs { get; set; }
        public ICollection<SosSystem> SosSystem { get; set; }
        public ICollection<Srcdoc> Srcdocs { get; set; }
        public ICollection<Fcrtpparchive> Fcrtpparchives { get; set; }
        public ICollection<Racertsystem> Racertsystems { get; set; }
        public ICollection<Tpparchive> Tpparchives { get; set; }
        public ICollection<Vendordoc> Vendordocs { get; set; }
    }
}
