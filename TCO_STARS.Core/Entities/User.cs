﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class User
    {
        public User()
        {
            Asbuiltlink = new HashSet<Asbuiltlink>();
            Commdossier = new HashSet<Commdossier>();
            Docsad = new HashSet<Docsad>();
            DocumentnoLetuser = new HashSet<Documentno>();
            DocumentnoUpdateuser = new HashSet<Documentno>();
            Fcrqvdarchive = new HashSet<Fcrqvdarchive>();
            Form141Signuser = new HashSet<Form141>();
            Form141Updateuser = new HashSet<Form141>();
            Qvdarchive = new HashSet<Qvdarchive>();
            Scopedrwgrev = new HashSet<Scopedrwgrev>();
            SosPeople = new HashSet<SosPeople>();
            Docfilelinks = new HashSet<Docfilelink>();
            Dossierreviewcontractors = new HashSet<Dossierreview>();
            Pfds = new HashSet<Dossierreview>();
            Dossierreviews = new HashSet<Dossierreview>();
            Punchlistspostverified = new HashSet<Punchlistspost>();
            Punchlistspostraised = new HashSet<Punchlistspost>();
            QvdarchiveVerified = new HashSet<Qvdarchive>();
            ScopedrwgrevApd = new HashSet<Scopedrwgrev>();
            SosSystemSign = new HashSet<SosSystem>();
        }

        public int Userid { get; set; }
        public string Usercai { get; set; }
        public string Fullname { get; set; }
        public int? Usertypeid { get; set; }
        public string Canapprove { get; set; }
        public int? Closed { get; set; }
        public string Email { get; set; }
        public string Badge { get; set; }
        public string Location { get; set; }
        public string Password { get; set; }
        public int? Usergroupid { get; set; }
        public string CyrName { get; set; }
        public string SpeakLang { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public int? Tcopositionid { get; set; }
        public string Trainingstatus { get; set; }
        public string Comments { get; set; }
        public DateTime? LastLogonDate { get; set; }

        public ICollection<Asbuiltlink> Asbuiltlink { get; set; }
        public ICollection<Commdossier> Commdossier { get; set; }
        public ICollection<Docsad> Docsad { get; set; }
        public ICollection<Docfilelink> Docfilelinks { get; set; }
        public ICollection<Documentno> DocumentnoLetuser { get; set; }
        public ICollection<Documentno> DocumentnoUpdateuser { get; set; }
        public ICollection<Dossierreview> Dossierreviewcontractors { get; set; }
        public ICollection<Dossierreview> Pfds { get; set; }
        public ICollection<Dossierreview> Dossierreviews { get; set; }
        public ICollection<Fcrqvdarchive> Fcrqvdarchive { get; set; }
        public ICollection<Form141> Form141Signuser { get; set; }
        public ICollection<Form141> Form141Updateuser { get; set; }
        public ICollection<Qvdarchive> Qvdarchive { get; set; }
        public ICollection<Scopedrwgrev> Scopedrwgrev { get; set; }
        public ICollection<SosPeople> SosPeople { get; set; }
        public ICollection<Punchlistspost> Punchlistspostverified { get; set; }
        public ICollection<Punchlistspost> Punchlistspostraised { get; set; }
        public ICollection<Qvdarchive> QvdarchiveVerified { get; set; }
        public ICollection<Scopedrwgrev> ScopedrwgrevApd { get; set; }
        public ICollection<SosSystem> SosSystemSign { get; set; }
    }
}
