﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Scopedrwgrev
    {
        public Scopedrwgrev()
        {
            Scopedrwgrevsyslink = new HashSet<Scopedrwgrevsyslink>();
        }

        public int Id { get; set; }
        public int Scopedrwgsheetid { get; set; }
        public string Tnvrev { get; set; }
        public string Drwgrev { get; set; }
        public DateTime? Issuedate { get; set; }
        public DateTime? Apddate { get; set; }
        public string Filename { get; set; }
        public int? Apduserid { get; set; }
        public decimal? Haveorig { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Comments { get; set; }

        public Scopedrwgsheet Scopedrwgsheet { get; set; }
        public User Updateuser { get; set; }
        public User Apduser { get; set; }
        public ICollection<Scopedrwgrevsyslink> Scopedrwgrevsyslink { get; set; }
    }
}
