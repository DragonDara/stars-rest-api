﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Component
    {
        public int? Systemid { get; set; }
        public int? Loopid { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Tagid { get; set; }
        public int? Parenttagid { get; set; }
        public int? Starsstatus { get; set; }
        public int? Updateuserid { get; set; }
        public int Id { get; set; }

        public Componentno Loop { get; set; }
        public Componentno ParentComponent { get; set; }
        public Systema System { get; set; }

        public Componentno Componentno { get; set; }
    }
}
