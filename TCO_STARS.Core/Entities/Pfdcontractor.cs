﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Pfdcontractor
    {
        public Pfdcontractor()
        {
            IbArchive = new HashSet<IbArchive>();
        }

        public string Contractno { get; set; }
        public string Company { get; set; }

        public ICollection<IbArchive> IbArchive { get; set; }
    }
}
