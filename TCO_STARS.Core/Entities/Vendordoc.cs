﻿namespace TCO_STARS.Core.Entities
{
    public partial class Vendordoc
    {
        public int Systemid { get; set; }
        public int Tcodocnoid { get; set; }
        public int Id { get; set; }

        public Documentno Tcodocno { get; set; }
        public Systema System { get; set; }
    }
}
