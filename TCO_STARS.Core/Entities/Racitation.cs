﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Racitation
    {
        public Racitation()
        {
            Racitationfile = new HashSet<Racitationfile>();
        }

        public string Tcno { get; set; }
        public string Agency { get; set; }
        public string Issuedate { get; set; }
        public string Inspector { get; set; }
        public string Type { get; set; }
        public string Coc { get; set; }
        public string Itemno { get; set; }
        public string Comments { get; set; }
        public string Raperson { get; set; }
        public string Department { get; set; }
        public string Clearpr { get; set; }
        public string Contact { get; set; }
        public string Updatedate { get; set; }
        public string Statuspen { get; set; }
        public string Letterdate { get; set; }
        public int? Status { get; set; }
        public string So { get; set; }
        public string Areamanager { get; set; }
        public int? Updateuserid { get; set; }
        public int? Closed { get; set; }
        public int? Areaid { get; set; }
        public int Id { get; set; }

        public Area Area { get; set; }
        public ICollection<Racitationfile> Racitationfile { get; set; }
    }
}
