﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Fcrtpparchive
    {
        public Fcrtpparchive()
        {
            Fcrtpparchivelink = new HashSet<Fcrtpparchivelink>();
        }

        public int? Discipline { get; set; }
        public int? Systemid { get; set; }
        public string Tagno { get; set; }
        public DateTime? Receiveddate { get; set; }
        public string Tcopipeowner { get; set; }
        public DateTime? Sendsigndate { get; set; }
        public DateTime? Signoffdate { get; set; }
        public DateTime? Transmitdate { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Comments { get; set; }
        public DateTime? Sendtopfd { get; set; }
        public decimal? Showdrwg { get; set; }
        public string Drawingno { get; set; }
        public string Linenumber { get; set; }
        public string Drawingno2 { get; set; }
        public int? Tmpf { get; set; }
        public DateTime? Accepteddate { get; set; }
        public string Addcomments { get; set; }
        public DateTime? Tidelinkputinstarsdate { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
        public ICollection<Fcrtpparchivelink> Fcrtpparchivelink { get; set; }
    }
}
