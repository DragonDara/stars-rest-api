﻿using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Flsdoctype
    {
        public Flsdoctype()
        {
            Pmsdocdetail = new HashSet<Pmsdocdetail>();
        }

        public int Id { get; set; }
        public string Doctype { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal? Overall { get; set; }
        public string Unit { get; set; }
        public int? Sortorder { get; set; }

        public ICollection<Pmsdocdetail> Pmsdocdetail { get; set; }
    }
}
