﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Form141
    {
        public int? Systemid { get; set; }
        public int? Itemid { get; set; }
        public int? Reqbconst { get; set; }
        public int? Reqbstartup { get; set; }
        public int? Reqastartup { get; set; }
        public string Tcodepperson { get; set; }
        public string Tcodeppersonrus { get; set; }
        public string Personname { get; set; }
        public DateTime? Signdate { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Updateuserid { get; set; }
        public string Newitemname { get; set; }
        public string Newitemnamerus { get; set; }
        public int? Signuserid { get; set; }
        public int? Tcopositionid { get; set; }
        public int? Doauserid { get; set; }
        public int Id { get; set; }

        public User Signuser { get; set; }
        public Systema System { get; set; }
        public User Updateuser { get; set; }
    }
}
