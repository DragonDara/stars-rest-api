﻿namespace TCO_STARS.Core.Entities
{
    public partial class Engdoc
    {
        public int Systemid { get; set; }
        public int Docnoid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Systema System { get; set; }
    }
}
