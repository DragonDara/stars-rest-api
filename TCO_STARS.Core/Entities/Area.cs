﻿using System;
using System.Collections.Generic;

namespace TCO_STARS.Core.Entities
{
    public partial class Area : BaseEntity
    {
        public Area()
        {
            Docsad = new HashSet<Docsad>();
            Peers = new HashSet<Peer>();
            Racitations = new HashSet<Racitation>();
            Units = new HashSet<Unit>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Closed { get; set; }
        public string Responsible { get; set; }
        public string Descrus { get; set; }
        public string Namerus { get; set; }
        public DateTime? Commdate { get; set; }
        public int? Aoc1 { get; set; }
        public int? Aoc2 { get; set; }
        public int? Schdocid { get; set; }
        public string Schedules { get; set; }
        public string Commreports { get; set; }
        public string Areaview { get; set; }
        public string Pcogendocs { get; set; }

        public ICollection<Docsad> Docsad { get; set; }
        public ICollection<Peer> Peers { get; set; }
        public ICollection<Racitation> Racitations { get; set; }
        public ICollection<Unit> Units { get; set; }
    }
}
