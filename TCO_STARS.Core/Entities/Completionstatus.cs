﻿namespace TCO_STARS.Core.Entities
{
    public partial class Completionstatus
    {
        public int? Systemid { get; set; }
        public int? Apldone { get; set; }
        public int? Apltotal { get; set; }
        public int? Bpldone { get; set; }
        public int? Bpltotal { get; set; }
        public int? Loopsdone { get; set; }
        public int? Loopstotal { get; set; }
        public int? Ppdone { get; set; }
        public int? Pptotal { get; set; }
        public int? Qvddone { get; set; }
        public int? Qvdtotal { get; set; }
        public int? Vendorkadone { get; set; }
        public int? Vendorkatotal { get; set; }
        public int? Vreddone { get; set; }
        public int? Vredtotal { get; set; }
        public int? Eredtotal { get; set; }
        public int? Ereddone { get; set; }
        public int? Sd { get; set; }
        public int? Compnumbers { get; set; }
        public int? Commereddone { get; set; }
        public int? Commvreddone { get; set; }
        public int? Commcheckliststotal { get; set; }
        public int? Commchecklistsdone { get; set; }
        public int? Proccount { get; set; }
        public int? Proccountdone { get; set; }
        public int? VreddoneFpr { get; set; }
        public int? VredtotalFpr { get; set; }
        public int? EreddoneFpr { get; set; }
        public int? EredtotalFpr { get; set; }
        public int? Apldonecm { get; set; }
        public int? Apltotalcm { get; set; }
        public int? Bpldonecm { get; set; }
        public int? Bpltotalcm { get; set; }
        public int? Areaid { get; set; }
        public int? Vareaid { get; set; }
        public int? Tppdone { get; set; }
        public int? Tpptotal { get; set; }
        public int? Commcheckloaded { get; set; }
        public int? Radone { get; set; }
        public int? Ratotal { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
    }
}
