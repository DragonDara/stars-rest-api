﻿namespace TCO_STARS.Core.Entities
{
    public partial class Componentdocnolink
    {
        public int? Tagid { get; set; }
        public int? Docnoid { get; set; }
        public int Id { get; set; }

        public Documentno Docno { get; set; }
        public Componentno Tag { get; set; }
    }
}
