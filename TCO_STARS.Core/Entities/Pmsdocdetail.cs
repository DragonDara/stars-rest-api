﻿namespace TCO_STARS.Core.Entities
{
    public partial class Pmsdocdetail
    {
        public int Flsdocdetailid { get; set; }
        public int Flsdoctypeid { get; set; }
        public int Systemid { get; set; }
        public string DocNo { get; set; }
        public string EngName { get; set; }
        public string RusName { get; set; }
        public string FilePath { get; set; }
        public string RevComNo { get; set; }
        public int? SortOrder { get; set; }
        public string Description { get; set; }
        public string Tagno { get; set; }
        public string Checksheetno { get; set; }
        public string FilePathR { get; set; }
        public decimal? Transfertotide { get; set; }
        public string Pmsdoctype { get; set; }
        public string Iblocation { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public int Id { get; set; }

        public Flsdoctype Flsdoctype { get; set; }
        public Pmssystem System { get; set; }
    }
}
