﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Valveworkshop
    {
        public string Jobno { get; set; }
        public string Valveno { get; set; }
        public string Location { get; set; }
        public string Serialno { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Classification { get; set; }
        public DateTime? Certdate { get; set; }
        public DateTime? Nextcertdate { get; set; }
        public int? Disciplineid { get; set; }
        public string Docno { get; set; }
        public int? Docnoid { get; set; }
        public string Filename { get; set; }
        public DateTime? Transferredtotide { get; set; }
        public string Booklocation { get; set; }
        public int Id { get; set; }

        public Discipline Discipline { get; set; }
    }
}
