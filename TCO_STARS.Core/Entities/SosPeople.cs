﻿namespace TCO_STARS.Core.Entities
{
    public partial class SosPeople
    {
        public int? Sositemid { get; set; }
        public int? Userid { get; set; }
        public int Id { get; set; }

        public Sositem Sositem { get; set; }
        public User User { get; set; }
    }
}
