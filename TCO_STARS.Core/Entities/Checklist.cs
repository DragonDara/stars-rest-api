﻿using System;

namespace TCO_STARS.Core.Entities
{
    public partial class Checklist
    {
        public int? Systemid { get; set; }
        public string Tagno { get; set; }
        public string Checksheet { get; set; }
        public DateTime? Datecompleted { get; set; }
        public string Itemstatus { get; set; }
        public string Nameinsp { get; set; }
        public int? Discipline { get; set; }
        public string Engstatus { get; set; }
        public DateTime? Createddate { get; set; }
        public int? Createdweek { get; set; }
        public string Asheetcomplete { get; set; }
        public string Servdesc { get; set; }
        public string Sheetdesc { get; set; }
        public string Filename { get; set; }
        public int? TppFlag { get; set; }
        public int? Starsstatus { get; set; }
        public int? Updateuserid { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Comments { get; set; }
        public int? Remediation { get; set; }
        public int Id { get; set; }

        public Systema System { get; set; }
    }
}
