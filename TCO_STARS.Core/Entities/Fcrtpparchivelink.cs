﻿namespace TCO_STARS.Core.Entities
{
    public partial class Fcrtpparchivelink
    {
        public int? Tppid { get; set; }
        public string Tpplink { get; set; }
        public int? Filetype { get; set; }
        public int Id { get; set; }

        public Fcrtpparchive Tpp { get; set; }
    }
}
