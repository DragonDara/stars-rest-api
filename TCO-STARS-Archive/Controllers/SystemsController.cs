﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TCO_STARS.Core.Entities;
using TCO_STARS.Infrastructure.Data;

namespace TCO_STARS.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SystemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Systems
        [HttpGet]
        [EnableQuery()]
        public IEnumerable<Systema> GetSystems()
        {
            return _context.Systems;
        }

        // GET: api/Systems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSystema([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var systema = await _context.Systems.FindAsync(id);

            if (systema == null)
            {
                return NotFound();
            }

            return Ok(systema);
        }

        // PUT: api/Systems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSystema([FromRoute] int id, [FromBody] Systema systema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != systema.Systemid)
            {
                return BadRequest();
            }

            _context.Entry(systema).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SystemaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Systems
        [HttpPost]
        public async Task<IActionResult> PostSystema([FromBody] Systema systema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Systems.Add(systema);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSystema", new { id = systema.Systemid }, systema);
        }

        // DELETE: api/Systems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSystema([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var systema = await _context.Systems.FindAsync(id);
            if (systema == null)
            {
                return NotFound();
            }

            _context.Systems.Remove(systema);
            await _context.SaveChangesAsync();

            return Ok(systema);
        }

        private bool SystemaExists(int id)
        {
            return _context.Systems.Any(e => e.Systemid == id);
        }
    }
}