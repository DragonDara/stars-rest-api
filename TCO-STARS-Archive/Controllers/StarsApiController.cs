using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TCO_STARS.Core.Entities;
using TCO_STARS.Infrastructure.Data;

namespace TCO_STARS_Archive.Controllers
{
    [Route("api/stars")]
    [ApiController]
    public class StarsApiController : ControllerBase
    {
        private readonly IEntriesProvider provider;
        public StarsApiController(IEntriesProvider provider)
        {
            this.provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        [HttpGet("{type}")]
        [EnableQuery(MaxExpansionDepth = 4, EnableCorrelatedSubqueryBuffering = true)]
        public IActionResult Entries(string type)
        {
            if (string.IsNullOrEmpty(type)) throw new ArgumentNullException(nameof(type));

            var serverType = ServerType(type);
            if (serverType is null) return NotFound();

            return Ok(Entries(serverType));
        }

        [HttpGet("count/{type}")]
        public async Task<IActionResult> CountAsync(string type, ODataQueryOptions<Systema> options)
        {
            if (string.IsNullOrEmpty(type)) throw new ArgumentNullException(nameof(type));

            var serverType = ServerType(type);
            if (serverType is null) return NotFound();

            var queryContext = new ODataQueryContext(options.Context.Model, serverType, options.Context.Path);
            var queryOptions = new ODataQueryOptions(queryContext, Request);

            var entries = Entries(serverType);

            var result = queryOptions.ApplyTo(entries) as IQueryable<object>;

            return Ok(await result?.CountAsync());
        }

        private Type ServerType(string type)
        {
            var assembly = typeof(Systema).Assembly;
            return assembly.GetType($"TCO_STARS.Core.Entities.{type}");
        }

        private IQueryable<object> Entries(Type type)
        {
            var methodInfo = typeof(EntriesProvider).GetMethod("Entries").MakeGenericMethod(type);
            return methodInfo.Invoke(provider, null) as IQueryable<object>;
        }
    }
}